﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.DataBase;
using SistemaLineaB.Clases;

namespace SistemaLineaB
{
    public partial class Buscador : Form
    {
        ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();

        public Buscador()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (txtorden.Text == "")
            {
                MessageBox.Show("Por favor ingrese una orden");
            }else
            {
                int orden = Convert.ToInt32(txtorden.Text);
                var datosBusqueda = BuscarOrden(orden);
                if(datosBusqueda == null)
                {
                    MessageBox.Show("El cristerio de busqueda no existe");
                }else
                {
                   // dgvBuscador.DataSource = null;
                   // dgvBuscador.Columns.Clear();
                    dgvBuscador.DataSource = datosBusqueda;


                }
                
            }
        }

        private void Buscador_Load(object sender, EventArgs e)
        {
            var datosbuscador = BuscarOrden();
          dgvBuscador.DataSource = datosbuscador;
            
    
        }

        private object BuscarOrden(long NumOrden)
       {

            var buscar = (from t in db.view_Buscadors
                          where t.orden.Equals(NumOrden)
                          select t).ToList();

            return buscar;

        }

        private object BuscarOrden()
        {

            var buscar = (from b in db.view_Buscadors
                           select b).ToList();

            return buscar;
        }

        private void txtorden_TextChanged_1(object sender, EventArgs e)
        {
            if (txtorden.Text == "")
            {
                var datosBusqueda = BuscarOrden();
                dgvBuscador.DataSource = datosBusqueda;
            }
            else
            {
                long orden = Convert.ToInt64(txtorden.Text);
                var datosBusqueda = BuscarOrden(orden);
                dgvBuscador.DataSource = datosBusqueda;

            }
        }
    }
    }
