﻿namespace SistemaLineaB
{
    partial class Formulario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulario));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.crearOrdenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearClienteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearOrdenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tallerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diagnoticoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reparacionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscardorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bandejaServicioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.despachoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.recepcionEquipoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblNombreUsuario = new System.Windows.Forms.Label();
            this.logo = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearOrdenToolStripMenuItem,
            this.tallerToolStripMenuItem,
            this.buscardorToolStripMenuItem,
            this.serviciosToolStripMenuItem,
            this.reporteToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(856, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // crearOrdenToolStripMenuItem
            // 
            this.crearOrdenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearClienteToolStripMenuItem,
            this.crearOrdenToolStripMenuItem1});
            this.crearOrdenToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crearOrdenToolStripMenuItem.Name = "crearOrdenToolStripMenuItem";
            this.crearOrdenToolStripMenuItem.Size = new System.Drawing.Size(69, 21);
            this.crearOrdenToolStripMenuItem.Text = "Registro";
            // 
            // crearClienteToolStripMenuItem
            // 
            this.crearClienteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearClienteToolStripMenuItem1,
            this.actualizarClienteToolStripMenuItem,
            this.eliminarClienteToolStripMenuItem,
            this.consultaClienteToolStripMenuItem});
            this.crearClienteToolStripMenuItem.Name = "crearClienteToolStripMenuItem";
            this.crearClienteToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.crearClienteToolStripMenuItem.Text = "Cliente";
            // 
            // crearClienteToolStripMenuItem1
            // 
            this.crearClienteToolStripMenuItem1.Name = "crearClienteToolStripMenuItem1";
            this.crearClienteToolStripMenuItem1.Size = new System.Drawing.Size(175, 22);
            this.crearClienteToolStripMenuItem1.Text = "Crear Cliente";
            this.crearClienteToolStripMenuItem1.Click += new System.EventHandler(this.crearClienteToolStripMenuItem1_Click);
            // 
            // actualizarClienteToolStripMenuItem
            // 
            this.actualizarClienteToolStripMenuItem.Name = "actualizarClienteToolStripMenuItem";
            this.actualizarClienteToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.actualizarClienteToolStripMenuItem.Text = "Actualizar Cliente";
            this.actualizarClienteToolStripMenuItem.Click += new System.EventHandler(this.actualizarClienteToolStripMenuItem_Click);
            // 
            // eliminarClienteToolStripMenuItem
            // 
            this.eliminarClienteToolStripMenuItem.Name = "eliminarClienteToolStripMenuItem";
            this.eliminarClienteToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.eliminarClienteToolStripMenuItem.Text = "Eliminar Cliente";
            this.eliminarClienteToolStripMenuItem.Click += new System.EventHandler(this.eliminarClienteToolStripMenuItem_Click);
            // 
            // consultaClienteToolStripMenuItem
            // 
            this.consultaClienteToolStripMenuItem.Name = "consultaClienteToolStripMenuItem";
            this.consultaClienteToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.consultaClienteToolStripMenuItem.Text = "Consulta Cliente";
            this.consultaClienteToolStripMenuItem.Click += new System.EventHandler(this.consultaClienteToolStripMenuItem_Click);
            // 
            // crearOrdenToolStripMenuItem1
            // 
            this.crearOrdenToolStripMenuItem1.Name = "crearOrdenToolStripMenuItem1";
            this.crearOrdenToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.crearOrdenToolStripMenuItem1.Text = "Crear Orden";
            this.crearOrdenToolStripMenuItem1.Click += new System.EventHandler(this.crearOrdenToolStripMenuItem1_Click);
            // 
            // tallerToolStripMenuItem
            // 
            this.tallerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.diagnoticoToolStripMenuItem,
            this.reparacionToolStripMenuItem});
            this.tallerToolStripMenuItem.Name = "tallerToolStripMenuItem";
            this.tallerToolStripMenuItem.Size = new System.Drawing.Size(48, 21);
            this.tallerToolStripMenuItem.Text = "Taller";
            // 
            // diagnoticoToolStripMenuItem
            // 
            this.diagnoticoToolStripMenuItem.Name = "diagnoticoToolStripMenuItem";
            this.diagnoticoToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.diagnoticoToolStripMenuItem.Text = "Diagnotico";
            this.diagnoticoToolStripMenuItem.Click += new System.EventHandler(this.diagnoticoToolStripMenuItem_Click);
            // 
            // reparacionToolStripMenuItem
            // 
            this.reparacionToolStripMenuItem.Name = "reparacionToolStripMenuItem";
            this.reparacionToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.reparacionToolStripMenuItem.Text = "Reparacion ";
            this.reparacionToolStripMenuItem.Click += new System.EventHandler(this.reparacionToolStripMenuItem_Click);
            // 
            // buscardorToolStripMenuItem
            // 
            this.buscardorToolStripMenuItem.Name = "buscardorToolStripMenuItem";
            this.buscardorToolStripMenuItem.Size = new System.Drawing.Size(66, 21);
            this.buscardorToolStripMenuItem.Text = "Consulta";
            this.buscardorToolStripMenuItem.Click += new System.EventHandler(this.buscardorToolStripMenuItem_Click);
            // 
            // serviciosToolStripMenuItem
            // 
            this.serviciosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bandejaServicioToolStripMenuItem1,
            this.despachoToolStripMenuItem1,
            this.recepcionEquipoToolStripMenuItem1});
            this.serviciosToolStripMenuItem.Name = "serviciosToolStripMenuItem";
            this.serviciosToolStripMenuItem.Size = new System.Drawing.Size(65, 21);
            this.serviciosToolStripMenuItem.Text = "Servicios";
            // 
            // bandejaServicioToolStripMenuItem1
            // 
            this.bandejaServicioToolStripMenuItem1.Name = "bandejaServicioToolStripMenuItem1";
            this.bandejaServicioToolStripMenuItem1.Size = new System.Drawing.Size(169, 22);
            this.bandejaServicioToolStripMenuItem1.Text = "Bandeja servicio";
            this.bandejaServicioToolStripMenuItem1.Click += new System.EventHandler(this.bandejaServicioToolStripMenuItem1_Click);
            // 
            // despachoToolStripMenuItem1
            // 
            this.despachoToolStripMenuItem1.Name = "despachoToolStripMenuItem1";
            this.despachoToolStripMenuItem1.Size = new System.Drawing.Size(169, 22);
            this.despachoToolStripMenuItem1.Text = "Despacho";
            this.despachoToolStripMenuItem1.Click += new System.EventHandler(this.despachoToolStripMenuItem1_Click);
            // 
            // recepcionEquipoToolStripMenuItem1
            // 
            this.recepcionEquipoToolStripMenuItem1.Name = "recepcionEquipoToolStripMenuItem1";
            this.recepcionEquipoToolStripMenuItem1.Size = new System.Drawing.Size(169, 22);
            this.recepcionEquipoToolStripMenuItem1.Text = "Recepcion Equipo";
            this.recepcionEquipoToolStripMenuItem1.Click += new System.EventHandler(this.recepcionEquipoToolStripMenuItem1_Click);
            // 
            // reporteToolStripMenuItem
            // 
            this.reporteToolStripMenuItem.Name = "reporteToolStripMenuItem";
            this.reporteToolStripMenuItem.Size = new System.Drawing.Size(60, 21);
            this.reporteToolStripMenuItem.Text = "Reporte";
            this.reporteToolStripMenuItem.Click += new System.EventHandler(this.reporteToolStripMenuItem_Click);
            // 
            // lblNombreUsuario
            // 
            this.lblNombreUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblNombreUsuario.Location = new System.Drawing.Point(576, 0);
            this.lblNombreUsuario.Name = "lblNombreUsuario";
            this.lblNombreUsuario.Size = new System.Drawing.Size(280, 23);
            this.lblNombreUsuario.TabIndex = 2;
            this.lblNombreUsuario.Text = "Usuario:";
            // 
            // logo
            // 
            this.logo.Image = ((System.Drawing.Image)(resources.GetObject("logo.Image")));
            this.logo.Location = new System.Drawing.Point(53, 41);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(791, 360);
            this.logo.TabIndex = 1;
            // 
            // Formulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 424);
            this.Controls.Add(this.lblNombreUsuario);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Formulario";
            this.Text = "MENU";
            this.Load += new System.EventHandler(this.Formulario_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem crearOrdenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tallerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diagnoticoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reparacionToolStripMenuItem;
        private System.Windows.Forms.Label logo;
        private System.Windows.Forms.ToolStripMenuItem buscardorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearOrdenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem crearClienteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem actualizarClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarClienteToolStripMenuItem;
        private System.Windows.Forms.Label lblNombreUsuario;
        private System.Windows.Forms.ToolStripMenuItem serviciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bandejaServicioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem despachoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem recepcionEquipoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultaClienteToolStripMenuItem;
    }
}