﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.Clases;
using SistemaLineaB.DataBase;



namespace SistemaLineaB
{
    public partial class EliminarCliente : Form
    {
        ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();
        public EliminarCliente()
        {
            InitializeComponent();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            RCliente rc = new RCliente();

            int codigo = Convert.ToInt32(txtcodigo.Text);
            bool resultrado = rc.EliminarCliente(codigo);

            if (resultrado == true)
            {
                MessageBox.Show("El cliente Fue satisfatoriamente eliminado");
            }
            else
            {
                MessageBox.Show("El cliente no pudo eliminarse ");
            }
        }



        private void btnBuscar_Click(object sender, EventArgs e)
        {
            int codigo = Convert.ToInt32(txtcodigo.Text);
            BuscarCliente(codigo);
            


        }
        public void BuscarCliente(int codigoCliente)
        {
            var buscar = (from p in db.clientes
                          where p.cliente_id.Equals(codigoCliente)
                          select p).FirstOrDefault();


            if (buscar != null)
            {
                txtcodigo.Text = Convert.ToString(buscar.cliente_id);
                txtNombre.Text = buscar.Nombre;
                txtApellido.Text = buscar.Apellido;
                txtDireccion.Text = buscar.Direccion;
                txtRNC.Text = buscar.RNC;
                txtTelefono.Text = buscar.Telefono_1;
                txtCedula.Text = buscar.Cedula;
                txtCelular.Text = buscar.Telefono_2;
                txtCorreo.Text = buscar.Correo;

            }
            else
            {
                MessageBox.Show("El cliente no existe");
            }
        }

    }
}

