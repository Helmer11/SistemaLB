﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.DataBase;
using SistemaLineaB.Clases;


namespace SistemaLineaB
{
    public partial class ActualizacionCliente : Form
    {
        public int codigoUsuario;
        ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();
        
        public ActualizacionCliente()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            int codigo = int.Parse(txtcodigo.Text);
            string nombre = txtNombre.Text;
            string apellido = txtApellido.Text;
            string cedula = txtCedula.Text;
            string rnc = txtRNC.Text;
            string direccion = txtDireccion.Text;
            string telefono = txtTelefono.Text;
            string celular = txtCelular.Text;
            string correo = txtCorreo.Text;
          

            RCliente cliente = new RCliente();
            bool updatecliente = cliente.ActualidacionCliente(codigo,nombre, apellido, cedula, rnc, direccion, telefono, celular, correo, codigoUsuario, DateTime.Now);


                if(updatecliente == true)
            {
                MessageBox.Show("El cliente fue actualizado");
            }
            else
            {
                MessageBox.Show("El cliente no pudo ser actualizado");
            }
            limpiarControler();


        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            
            int codigo = Convert.ToInt32(txtcodigo.Text);
            BuscarCliente(codigo);


        }

        public void BuscarCliente(int codigoCliente)
        {
            var buscar = (from p in db.clientes
                          where p.cliente_id.Equals(codigoCliente)
                          select p).FirstOrDefault();


            if (buscar != null)
            {
                txtcodigo.Text = Convert.ToString(buscar.cliente_id);
                txtNombre.Text = buscar.Nombre;
                txtApellido.Text = buscar.Apellido;
                txtDireccion.Text = buscar.Direccion;
                txtRNC.Text = buscar.RNC;
                txtTelefono.Text = buscar.Telefono_1;
                txtCedula.Text = buscar.Cedula;
                txtCelular.Text = buscar.Telefono_2;
                txtCorreo.Text = buscar.Correo;

            }
            else
            {
                MessageBox.Show("El cliente no existe");
            }
        }
        private void limpiarControler()
        {

            txtNombre.Clear();
            txtApellido.Clear();
            txtCedula.Clear();
            txtRNC.Clear();
            txtDireccion.Clear();
            txtTelefono.Clear();
            txtCelular.Clear();
            txtCorreo.Clear();
        }


    }
}
