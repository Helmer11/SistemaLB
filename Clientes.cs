﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.Clases;


namespace SistemaLineaB
{
    public partial class Clientes : Form
    {

        public int idUsuario;
        public Clientes()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            string nombre = txtNombre.Text;
            string apellido = txtApellido.Text;
            string cedula = txtCedula.Text;
            string rnc = txtRNC.Text;
            string direccion = txtDireccion.Text;
            string telefono = txtTelefono.Text;
            string celular = txtCelular.Text;
            string correo = txtCorreo.Text;

            Funciones f = new Funciones();

            RCliente client = new RCliente();

           bool cl =  client.CrearCliente(nombre, apellido, cedula, rnc, direccion, telefono, celular, correo, idUsuario, DateTime.Now);
            
            if( cl == true)
            {
                MessageBox.Show("El cliente fue  satifactoriamente creado");
            }
            else
            {
                MessageBox.Show("El cliente no pudo ser creado");
            }
            limpiarControler();

        }

        private void limpiarControler()
        {

             txtNombre.Clear();
             txtApellido.Clear();
             txtCedula.Clear();
             txtRNC.Clear();
            txtDireccion.Clear();
             txtTelefono.Clear();
            txtCelular.Clear();
             txtCorreo.Clear();
        }


    }
}
