﻿namespace SistemaLineaB
{
    partial class reparacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnbuscar = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtorden = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtmodelo = new System.Windows.Forms.TextBox();
            this.txtserie = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtmarca = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbxparte = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbxreparacion = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.rb_paraTrasladar = new System.Windows.Forms.RadioButton();
            this.rbno_reparado = new System.Windows.Forms.RadioButton();
            this.rbsoftware = new System.Windows.Forms.RadioButton();
            this.rbcambio = new System.Windows.Forms.RadioButton();
            this.txtcomentario = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cbProducto = new System.Windows.Forms.ComboBox();
            this.CbFechaVenta = new System.Windows.Forms.ComboBox();
            this.lbEstado = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbUbicacion = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.CbFechaVenta);
            this.panel1.Controls.Add(this.cbProducto);
            this.panel1.Controls.Add(this.btnbuscar);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtorden);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtmodelo);
            this.panel1.Controls.Add(this.txtserie);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtmarca);
            this.panel1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panel1.Location = new System.Drawing.Point(12, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(652, 122);
            this.panel1.TabIndex = 14;
            this.panel1.Tag = "Datos del equipo";
            // 
            // btnbuscar
            // 
            this.btnbuscar.Location = new System.Drawing.Point(198, 12);
            this.btnbuscar.Name = "btnbuscar";
            this.btnbuscar.Size = new System.Drawing.Size(98, 24);
            this.btnbuscar.TabIndex = 19;
            this.btnbuscar.Text = "Consultar";
            this.btnbuscar.UseVisualStyleBackColor = true;
            this.btnbuscar.Click += new System.EventHandler(this.btnbuscar_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(302, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Producto";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Orden Servicio:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 1;
            // 
            // txtorden
            // 
            this.txtorden.Location = new System.Drawing.Point(101, 15);
            this.txtorden.Name = "txtorden";
            this.txtorden.Size = new System.Drawing.Size(91, 20);
            this.txtorden.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(435, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Fecha Venta";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(221, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Serie";
            // 
            // txtmodelo
            // 
            this.txtmodelo.Location = new System.Drawing.Point(83, 56);
            this.txtmodelo.Name = "txtmodelo";
            this.txtmodelo.Size = new System.Drawing.Size(126, 20);
            this.txtmodelo.TabIndex = 9;
            // 
            // txtserie
            // 
            this.txtserie.Location = new System.Drawing.Point(256, 57);
            this.txtserie.Name = "txtserie";
            this.txtserie.Size = new System.Drawing.Size(147, 20);
            this.txtserie.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Modelo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(478, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Marca";
            // 
            // txtmarca
            // 
            this.txtmarca.Location = new System.Drawing.Point(519, 14);
            this.txtmarca.Name = "txtmarca";
            this.txtmarca.Size = new System.Drawing.Size(117, 20);
            this.txtmarca.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cbxparte);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.cbxreparacion);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.rb_paraTrasladar);
            this.panel2.Controls.Add(this.rbno_reparado);
            this.panel2.Controls.Add(this.rbsoftware);
            this.panel2.Controls.Add(this.rbcambio);
            this.panel2.Controls.Add(this.txtcomentario);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Location = new System.Drawing.Point(12, 214);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(652, 180);
            this.panel2.TabIndex = 15;
            // 
            // cbxparte
            // 
            this.cbxparte.FormattingEnabled = true;
            this.cbxparte.Location = new System.Drawing.Point(425, 45);
            this.cbxparte.Name = "cbxparte";
            this.cbxparte.Size = new System.Drawing.Size(213, 21);
            this.cbxparte.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(376, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "Parte";
            // 
            // cbxreparacion
            // 
            this.cbxreparacion.FormattingEnabled = true;
            this.cbxreparacion.Location = new System.Drawing.Point(85, 46);
            this.cbxreparacion.Name = "cbxreparacion";
            this.cbxreparacion.Size = new System.Drawing.Size(194, 21);
            this.cbxreparacion.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "Reparacion";
            // 
            // rb_paraTrasladar
            // 
            this.rb_paraTrasladar.AutoSize = true;
            this.rb_paraTrasladar.Location = new System.Drawing.Point(336, 24);
            this.rb_paraTrasladar.Name = "rb_paraTrasladar";
            this.rb_paraTrasladar.Size = new System.Drawing.Size(91, 17);
            this.rb_paraTrasladar.TabIndex = 10;
            this.rb_paraTrasladar.TabStop = true;
            this.rb_paraTrasladar.Text = "Para Traslado";
            this.rb_paraTrasladar.UseVisualStyleBackColor = true;
            // 
            // rbno_reparado
            // 
            this.rbno_reparado.AutoSize = true;
            this.rbno_reparado.Location = new System.Drawing.Point(233, 24);
            this.rbno_reparado.Name = "rbno_reparado";
            this.rbno_reparado.Size = new System.Drawing.Size(84, 17);
            this.rbno_reparado.TabIndex = 9;
            this.rbno_reparado.TabStop = true;
            this.rbno_reparado.Text = "No reparado";
            this.rbno_reparado.UseVisualStyleBackColor = true;
            // 
            // rbsoftware
            // 
            this.rbsoftware.AutoSize = true;
            this.rbsoftware.Location = new System.Drawing.Point(160, 22);
            this.rbsoftware.Name = "rbsoftware";
            this.rbsoftware.Size = new System.Drawing.Size(67, 17);
            this.rbsoftware.TabIndex = 8;
            this.rbsoftware.TabStop = true;
            this.rbsoftware.Text = "Software";
            this.rbsoftware.UseVisualStyleBackColor = true;
            // 
            // rbcambio
            // 
            this.rbcambio.AutoSize = true;
            this.rbcambio.Location = new System.Drawing.Point(94, 22);
            this.rbcambio.Name = "rbcambio";
            this.rbcambio.Size = new System.Drawing.Size(60, 17);
            this.rbcambio.TabIndex = 7;
            this.rbcambio.TabStop = true;
            this.rbcambio.Text = "Cambio";
            this.rbcambio.UseVisualStyleBackColor = true;
            // 
            // txtcomentario
            // 
            this.txtcomentario.Location = new System.Drawing.Point(12, 97);
            this.txtcomentario.Multiline = true;
            this.txtcomentario.Name = "txtcomentario";
            this.txtcomentario.Size = new System.Drawing.Size(427, 76);
            this.txtcomentario.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Comentario";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(112, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 13);
            this.label10.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Tipo de Accion";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(87, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Datos del equipo";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 198);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "Reparacion";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(604, 413);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 34);
            this.button1.TabIndex = 18;
            this.button1.Text = "Reparacion";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbProducto
            // 
            this.cbProducto.FormattingEnabled = true;
            this.cbProducto.Location = new System.Drawing.Point(351, 18);
            this.cbProducto.Name = "cbProducto";
            this.cbProducto.Size = new System.Drawing.Size(121, 21);
            this.cbProducto.TabIndex = 20;
            // 
            // CbFechaVenta
            // 
            this.CbFechaVenta.FormattingEnabled = true;
            this.CbFechaVenta.Location = new System.Drawing.Point(512, 56);
            this.CbFechaVenta.Name = "CbFechaVenta";
            this.CbFechaVenta.Size = new System.Drawing.Size(121, 21);
            this.CbFechaVenta.TabIndex = 21;
            // 
            // lbEstado
            // 
            this.lbEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEstado.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbEstado.Location = new System.Drawing.Point(532, 18);
            this.lbEstado.Name = "lbEstado";
            this.lbEstado.Size = new System.Drawing.Size(132, 23);
            this.lbEstado.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(453, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 23);
            this.label4.TabIndex = 20;
            this.label4.Text = "Estado:";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(403, 190);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 23);
            this.label16.TabIndex = 21;
            this.label16.Text = "Ubicacion:";
            // 
            // lbUbicacion
            // 
            this.lbUbicacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUbicacion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbUbicacion.Location = new System.Drawing.Point(491, 188);
            this.lbUbicacion.Name = "lbUbicacion";
            this.lbUbicacion.Size = new System.Drawing.Size(173, 23);
            this.lbUbicacion.TabIndex = 22;
            // 
            // reparacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 459);
            this.Controls.Add(this.lbUbicacion);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbEstado);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "reparacion";
            this.Text = "Reparacion";
            this.Load += new System.EventHandler(this.reparacion_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtorden;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtmodelo;
        private System.Windows.Forms.TextBox txtserie;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtmarca;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtcomentario;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbxparte;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbxreparacion;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RadioButton rb_paraTrasladar;
        private System.Windows.Forms.RadioButton rbno_reparado;
        private System.Windows.Forms.RadioButton rbsoftware;
        private System.Windows.Forms.RadioButton rbcambio;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnbuscar;
        private System.Windows.Forms.ComboBox CbFechaVenta;
        private System.Windows.Forms.ComboBox cbProducto;
        private System.Windows.Forms.Label lbEstado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbUbicacion;
    }
}