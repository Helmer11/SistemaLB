﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.DataBase;
using SistemaLineaB.Clases;

namespace SistemaLineaB
{
    public partial class Despacho : Form
    {

        ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();

        public Despacho()
        {
            InitializeComponent();
        }
        public void buscarOrden(int orden)
        {
            var query = (from d in db.ordenOperacions
                         where d.orden_id.Equals(orden) && d.estado_equipo.Equals(EstadoEquipo.Despachado)
                         select d).FirstOrDefault();

            if( query != null)
            {
                cbProducto.Text = query.tipo_producto.Descripcion_tipo_producto;
                txtmarca.Text = query.marca.nombre_marca;
                txtmodelo.Text = query.modelo.nombre_modelo;
                txtserie.Text = query.serie;
                CbFechaVenta.SelectedText = query.fecha_venta.ToString();
            }
            else
            {
                MessageBox.Show("No existe orden con su criterio");
            }
           
        }


        public enum  EstadoEquipo
        {
            Despachado = 4,
            RecepcionEquipo = 5,
        };

        private void button1_Click(object sender, EventArgs e)
        {

        }

        public Boolean GuardarDespacho()
        {

            try
            {
                ordenoperacion_diagnostico od = new ordenoperacion_diagnostico();
                ordenOperacion op = new ordenOperacion();

                cbProducto.SelectedText = op.tipo_producto.Descripcion_tipo_producto;
                txtmarca.Text = op.marca.nombre_marca;
                txtmodelo.Text = op.modelo.nombre_modelo;
                txtserie.Text = op.serie;

                op.estado_equipo_id =(int) EstadoEquipo.Despachado;
                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            int orden = 0;
           
            if(txtorden.Text == "")
            {
                MessageBox.Show("Introduzca la orden");
            }else
            {
                 orden = Convert.ToInt32(txtorden.Text);
                buscarOrden(orden);
            }

         
        }

        public void Desabilitado(bool estado)
        {

            cbProducto.Enabled = estado;
            txtmarca.Enabled = estado;
            txtmodelo.Enabled = estado;
            txtserie.Enabled = estado;
            CbFechaVenta.Enabled = estado;

        }

    }




}
