﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.Clases;


namespace SistemaLineaB
{
    public partial class Login : Form
    {
        
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           String user =  txtUsuario.Text;
            String clave = txtClave.Text;

            Funciones fc = new Funciones();
            Formulario fm = new Formulario();
            int usuario_id = fc.Login(user, clave);



            if (usuario_id > 0)
            {
                fm.idUsuario = fc.cargar_usuario(usuario_id);
                fm.Show();
          
            }
            else
            {
                MessageBox.Show("Nombre de usuario o contraseña incorrecta");
            }



        }
    }
}
