﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.DataBase;
using SistemaLineaB.Clases;

namespace SistemaLineaB
{
    public partial class Formulario : Form
    {
        public int idUsuario;
        ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();

        public Formulario()
        {
            InitializeComponent();
        }
        private void Formulario_Load(object sender, EventArgs e)
        {
            cargar_usuario(idUsuario);
            ValidacionRoles(idUsuario);
 
        }
        private void diagnoticoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            diagnotico dg = new diagnotico();
            dg.Show();
        }

        private void reparacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reparacion rp = new reparacion();
            rp.Show();
        }

        private void buscardorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Buscador buscador = new Buscador();
            buscador.Show();
        }

        private void reporteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reporte reporte = new reporte();
            reporte.Show();
        }

        private void crearClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clientes cliente = new Clientes();
            cliente.Show();
        }


        public void cargar_usuario(int user)
        {
           
            var usuarioid = (from t in db.usuarios
                             where t.usuario_id.Equals(user)
                             select t).FirstOrDefault();

            lblNombreUsuario.Text = "Bienvenido: "+ usuarioid.Nombre.ToUpper() + " " + usuarioid.Apellido.ToUpper();
  
        }

        private void crearOrdenToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Ordenes os = new Ordenes();
            os.Show();
        }

        private void crearClienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Clientes cliente = new Clientes();
            cliente.Show();
            cliente.idUsuario = idUsuario;

        }

        private void actualizarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActualizacionCliente ac = new ActualizacionCliente();
            ac.Show();
            ac.codigoUsuario = idUsuario;
        }

        private void consultaClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultaCliente cc = new ConsultaCliente();
            cc.Show();
        }

        private void eliminarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EliminarCliente ec = new EliminarCliente();
            ec.Show();
        }

        private void despachoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Despacho des = new Despacho();
            des.Show();
        }

        private void recepcionEquipoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            RecepcionEquipo rq = new RecepcionEquipo();
            rq.Show();
        }

        private void bandejaServicioToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            BandejaServicio bs = new BandejaServicio();
            bs.Show();
        }
        public void ValidacionRoles( int user )
        {
            Roles_Validacion rv = new Roles_Validacion();
            bool resultado = rv.Rol_usuario(user);

            if(resultado == true)
            {
                reporteToolStripMenuItem.Enabled = false;
            }
           
        }

    


    }
}
