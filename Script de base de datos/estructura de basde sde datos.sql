USE [electrodomestico]
GO
/****** Object:  Table [dbo].[Cargo]    Script Date: 8/26/2017 3:11:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cargo](
	[cargo_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NULL,
	[descripcion] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[cargo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[clientes]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[clientes](
	[cliente_id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](20) NOT NULL,
	[Apellido] [varchar](20) NOT NULL,
	[Cedula] [varchar](20) NULL,
	[RNC] [varchar](20) NULL,
	[Direccion] [text] NULL,
	[Telefono_1] [varchar](20) NULL,
	[Telefono_2] [varchar](20) NULL,
	[Correo] [varchar](50) NULL,
	[usuario_creador] [int] NOT NULL,
	[Fecha_ingreso] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[cliente_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[diagnosticos]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[diagnosticos](
	[diagnostico_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NULL,
	[descripcion_diagnostico] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[diagnostico_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[estado_equipo]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[estado_equipo](
	[estado_equipo_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NOT NULL,
	[descripcion] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[estado_equipo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[estado_ubicacion]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[estado_ubicacion](
	[estado_ubicacion_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NOT NULL,
	[descripcion] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[estado_ubicacion_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[falla]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[falla](
	[falla_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NOT NULL,
	[descripcion_falla] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[falla_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[garantia]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[garantia](
	[garantia_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NOT NULL,
	[meses_gatia] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[garantia_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[marca]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[marca](
	[marca_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NOT NULL,
	[nombre_marca] [varchar](30) NOT NULL,
	[descripcion] [varchar](30) NOT NULL,
	[fecha_ingreso] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[marca_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[modelo]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[modelo](
	[modelo_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NOT NULL,
	[nombre_modelo] [varchar](30) NOT NULL,
	[descripcion] [varchar](30) NOT NULL,
	[fecha_ingreso] [datetime] NOT NULL,
	[tipo_producto_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[modelo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ordenOperacion]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ordenOperacion](
	[orden_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NOT NULL,
	[marca_id] [int] NOT NULL,
	[modelo_id] [int] NOT NULL,
	[cliente_id] [int] NOT NULL,
	[serie] [varchar](50) NOT NULL,
	[falla_id] [int] NOT NULL,
	[fecha_fabricante] [datetime] NULL,
	[fecha_venta] [datetime] NOT NULL,
	[tipo_producto_id] [int] NOT NULL,
	[garantia_id] [int] NOT NULL,
	[estado_equipo_id] [int] NOT NULL,
	[estado_ubicacion_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[orden_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ordenoperacion_diagnostico]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ordenoperacion_diagnostico](
	[ordenoperacion_diagnostico_id] [int] IDENTITY(1,1) NOT NULL,
	[ordenoperacion_id] [int] NOT NULL,
	[producto_id] [int] NOT NULL,
	[marca_id] [int] NOT NULL,
	[modelo_id] [int] NOT NULL,
	[serie] [varchar](50) NOT NULL,
	[fecha_venta] [datetime] NOT NULL,
	[garantia] [varchar](40) NOT NULL,
	[falla_id] [int] NOT NULL,
	[comentario] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[ordenoperacion_diagnostico_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ordenoperacion_reparacion]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ordenoperacion_reparacion](
	[ordenoperacion_reparacion_id] [int] IDENTITY(1,1) NOT NULL,
	[ordenoperacion_id] [int] NOT NULL,
	[producto_id] [int] NOT NULL,
	[marca_id] [int] NOT NULL,
	[modelo_id] [int] NOT NULL,
	[serie] [varchar](50) NOT NULL,
	[fecha_venta] [datetime] NOT NULL,
	[cambio] [bit] NOT NULL,
	[software] [bit] NOT NULL,
	[no_reparado] [bit] NOT NULL,
	[para_trasladar] [bit] NOT NULL,
	[reparacion_id] [int] NOT NULL,
	[parte_id] [int] NOT NULL,
	[comentario] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[ordenoperacion_reparacion_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[partes]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[partes](
	[parte_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NOT NULL,
	[Descripcion_parte] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[parte_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[producto]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[producto](
	[producto_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NOT NULL,
	[descripcion_producto] [int] NOT NULL,
	[tipo_producto_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[producto_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[reparacion]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[reparacion](
	[repacion_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NULL,
	[descripcion_reparacion] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[repacion_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tipo_producto]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tipo_producto](
	[tipo_producto_id] [int] IDENTITY(1,1) NOT NULL,
	[activo] [bit] NOT NULL,
	[Descripcion_tipo_producto] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[tipo_producto_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 8/26/2017 3:11:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuarios](
	[usuario_id] [int] IDENTITY(1,1) NOT NULL,
	[Activo] [bit] NULL,
	[Usuario] [varchar](20) NOT NULL,
	[Clave] [varchar](20) NOT NULL,
	[Nombre] [varchar](20) NOT NULL,
	[Apellido] [varchar](20) NOT NULL,
	[STATUS] [bit] NULL,
	[Direccion] [text] NULL,
	[Telefono] [varchar](20) NULL,
	[Correo] [varchar](50) NULL,
	[fecha_ingreso] [datetime] NOT NULL,
	[cargo_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[usuario_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Cargo] ON 

INSERT [dbo].[Cargo] ([cargo_id], [activo], [descripcion]) VALUES (1, 1, N'Gerente')
INSERT [dbo].[Cargo] ([cargo_id], [activo], [descripcion]) VALUES (2, 1, N'Cajero')
INSERT [dbo].[Cargo] ([cargo_id], [activo], [descripcion]) VALUES (3, 1, N'Supervisor')
SET IDENTITY_INSERT [dbo].[Cargo] OFF
SET IDENTITY_INSERT [dbo].[clientes] ON 

INSERT [dbo].[clientes] ([cliente_id], [Nombre], [Apellido], [Cedula], [RNC], [Direccion], [Telefono_1], [Telefono_2], [Correo], [usuario_creador], [Fecha_ingreso]) VALUES (1, N'juan daniel ', N'santana lora', N'22100570986', N'ndf-45782', N'Calle 13 reparto rosa #5, las caobas', N'809996634772', N'7527456743574', N'daniel@gmail.com', 2, CAST(0x0000A7D60137428D AS DateTime))
INSERT [dbo].[clientes] ([cliente_id], [Nombre], [Apellido], [Cedula], [RNC], [Direccion], [Telefono_1], [Telefono_2], [Correo], [usuario_creador], [Fecha_ingreso]) VALUES (2, N'orquidea', N'santo', N'465423534535434534', N'gfjg543453453', N'cghfjhgb,cjmghbmvghmh', N'4564534563', N'76574563453', N'dfhfgjnhg@gmail.com', 2, CAST(0x0000A7D6013D6B13 AS DateTime))
INSERT [dbo].[clientes] ([cliente_id], [Nombre], [Apellido], [Cedula], [RNC], [Direccion], [Telefono_1], [Telefono_2], [Correo], [usuario_creador], [Fecha_ingreso]) VALUES (8, N'dfghjgh', N'fgjyghkgh', N'452424324', N'4534523453', N'xfchjghn xdfgh', N'80872432455.', N'523453748534534', N'fjfgnfghgsdhjtyfg@gm', 2, CAST(0x0000A7DA0136DB98 AS DateTime))
SET IDENTITY_INSERT [dbo].[clientes] OFF
SET IDENTITY_INSERT [dbo].[diagnosticos] ON 

INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (1, 1, N'NO FUNCIONA AURICULAR ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (4, 1, N'BATERIA INVALIDA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (5, 1, N'NO FUNCIONA CAMARA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (6, 1, N'CONECTOR DE SIM CARD DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (7, 1, N'CORTO CIRCUITO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (9, 1, N'NO FUNCIONA DISPLAY ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (11, 1, N'ENCIENDE SOLO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (12, 1, N'SE APAGA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (13, 1, N'SE REINICIA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (15, 1, N'SE CALIENTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (16, 1, N'FALLA NO DETECTADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (17, 1, N'HOUSING DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (18, 1, N'NO FUNCIONA INFRARROJO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (19, 1, N'NO FUNCIONA JOYSTICK ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (20, 1, N'NO FUNCIONA MICROFONO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (23, 1, N'NO GUARDA CONTACTOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (24, 1, N'NO GUARDA  FOTOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (25, 1, N'NO LEE SIM')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (26, 1, N'NO LEE MEMORY CARD')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (27, 1, N'NO REPARABLE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (28, 1, N'NO FUNCIONA VIBRADOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (29, 1, N'PISTA ABIERTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (30, 1, N'NO CARGA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (31, 1, N'NO RETIENE CARGA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (32, 1, N'NO FUNCIONA 3G')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (33, 1, N'REPASO DE SOLDADURAS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (34, 1, N'NO FUNCIONA RINGER ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (36, 1, N'SEÑAL BAJA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (37, 1, N'SOFTWARE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (38, 1, N'NO FUNCIONA TECLADO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (39, 1, N'NO FUNCIONA TECLAS LATERALES ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (41, 1, N'NO FUNCIONAN LLAMADAS (RUIDO)')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (43, 1, N'NO FUNCIONA TOUCH SCREEN')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (44, 1, N'NO ENCIENDE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (45, 1, N'SE FRIZA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (46, 1, N'NO DOA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (47, 1, N'NO DAP')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (48, 1, N'NO FUNCIONA BLUETOOTH ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (49, 1, N'FLEX AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (50, 1, N'NO CONECTA A LA PC')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (51, 1, N'WI FI NO CONECTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (52, 1, N'BACK COVER NO AJUSTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (53, 1, N'NO FUNCIONA LA RADIO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (54, 1, N'P/A AVERIADO SE CALIENTA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (55, 1, N'NO FUNCIONA VIBRADOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (56, 1, N'EQUIPO 72/30 GRAY MARKET')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (57, 1, N'NO FUNCIONA HANDS FREE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (58, 1, N'NO RECONOCE MEMORY CARD')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (59, 1, N'FRONT HOUSING ROTO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (60, 1, N'EQUIPO 72/30 FABRICANTE ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (61, 1, N'IRREPARABLE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (62, 1, N'COMPRESOR DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (63, 1, N'CONTROL REMOTO DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (64, 1, N'DISPLAY DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (65, 1, N'DRENAJE TAPADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (66, 1, N'OVERLOAD ABIERTO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (67, 1, N'PROBLEMA DE GAS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (68, 1, N'PROBLEMA DE INSTALACION')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (69, 1, N'PROBLEMA ELECTRICO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (70, 1, N'SOBRE CONSUMO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (71, 1, N'TARJETA DAÑADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (72, 1, N'BOMBA DAÑADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (73, 1, N'PANEL DE DISPLAY DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (74, 1, N'DRENAJE DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (75, 1, N'PROBLEMA ELECTRICO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (76, 1, N'TAPA DAÑADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (77, 1, N'TRANSMISION DAÑADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (78, 1, N'ABANICO DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (79, 1, N'BOMBILLO DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (80, 1, N'FUENTE DAÑADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (81, 1, N'MENBRANA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (82, 1, N'PANEL DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (83, 1, N'PROBLEMA DE PUERTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (84, 1, N'PROBLEMA ELECTRICO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (85, 1, N'PROBLEMA DE LECTOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (86, 1, N'PROBLEMA ELECTRICO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (87, 1, N'PROBLEMA TARJETA DE AUDIO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (88, 1, N'COMPRESOR DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (89, 1, N'CORTO CIRCUITO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (90, 1, N'DRENAJE TAPADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (91, 1, N'EMPAQUE DAÑADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (92, 1, N'FILTRO HIDRATADOR OBSTRUIDO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (93, 1, N'FUGA DE REFRIGERANTE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (95, 1, N'OVERLOAD ABIERTO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (96, 1, N'OXIDO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (97, 1, N'RELAY DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (98, 1, N'TERMOSTATO DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (99, 1, N'TIMER ABIERTO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (100, 1, N'VOLTAJE ALTO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (101, 1, N'VOLTAJE BAJO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (102, 1, N'PROBLEMA DE LECTOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (103, 1, N'PROBLEMA ELECTRICO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (104, 1, N'PROBLEMA TARJETA DE AUDIO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (105, 1, N'ALIMENTACION ELECTRICA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (106, 1, N'PROBLEMA EN TARJETA DE AUDIO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (107, 1, N'PROBLEMA EN TARJETA DE VIDEO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (108, 1, N'NO VOLTAJE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (109, 1, N'RESISTENCIA ABIERTA O QUEMADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (110, 1, N'CABLE DE CORRIENTE DEFECTUOSO')
GO
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (112, 1, N'PLANCHA ROTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (113, 1, N'BOMBILLO QUEMADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (114, 1, N'CORTO INTERNO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (115, 1, N'MANUBRIO ROTO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (116, 1, N'AURICULAR-NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (118, 1, N'AURICULAR-SONIDO BAJO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (119, 1, N'BATERIA INVALIDA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (120, 1, N'CAMARA NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (121, 1, N'CONECTOR DE SIM CARD DANADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (122, 1, N'CORTO CIRCUITO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (123, 1, N'DISPLAY-EN BLANCO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (124, 1, N'DISPLAY-NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (125, 1, N'DISPLAY-PIERDE DISPLAY')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (126, 1, N'ENCENDIDO-ENCIENDE SOLO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (127, 1, N'ENCENDIDO-SE APAGA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (128, 1, N'ENCENDIDO-SE REINICIA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (129, 1, N'EQUIPO BLOQUEADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (130, 1, N'EQUIPO CALIENTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (131, 1, N'FALLA NO DETECTADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (132, 1, N'HOUSING DANADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (133, 1, N'INFRARROJO NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (134, 1, N'JOYSTICK-NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (135, 1, N'MICROFONO NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (137, 1, N'MICROFONO-SONIDO BAJO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (138, 1, N'NO GRABA CONTACTOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (139, 1, N'NO GUARDA  FOTOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (140, 1, N'NO LEE SIM')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (141, 1, N'NO RECONOCE MEMORY CARD')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (142, 1, N'NO VIBRA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (143, 1, N'PISTA ABIERTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (144, 1, N'PROBLEMA DE CARGA-NO CARGA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (147, 1, N'REPASO DE SOLDADURAS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (148, 1, N'RINGER-NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (149, 1, N'RINGER-SONIDO BAJO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (150, 1, N'SEÑAL BAJA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (151, 1, N'TECLADO-NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (152, 1, N'TECLAS LATERALES-NO FUNCIONAN')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (153, 1, N'TRANSMISION-FALLA LLAMADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (154, 1, N'TRANSMISION-RUIDOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (156, 1, N'PROBLEMAS DE TOUCH SCREEN')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (157, 1, N'NO ENCIENDE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (158, 1, N'SE FRIZA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (159, 1, N'NO DOA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (160, 1, N'NO DAP')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (161, 1, N'BLUETOOTH NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (162, 1, N'FLEX AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (163, 1, N'NO CONECTA A LA PC')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (164, 1, N'WI FI NO CONECTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (165, 1, N'BACK COVER NO AJUSTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (166, 1, N'TRANSMISOR DE RADIO AVERIDADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (167, 1, N'P/A AVERIADO SE CALIENTA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (168, 1, N'NO FUNCIONA VIBRADOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (169, 1, N'EQUIPO 72/30 GRAY MARKET')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (170, 1, N'EQUIPO 72/30 FABRICANTE ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (171, 1, N'AURICULAR-NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (173, 1, N'AURICULAR-SONIDO BAJO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (174, 1, N'BATERIA INVALIDA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (175, 1, N'CAMARA NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (176, 1, N'CONECTOR DE SIM CARD DANADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (177, 1, N'CORTO CIRCUITO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (178, 1, N'DISPLAY-EN BLANCO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (179, 1, N'DISPLAY-NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (180, 1, N'DISPLAY-PIERDE DISPLAY')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (181, 1, N'ENCENDIDO-ENCIENDE SOLO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (182, 1, N'ENCENDIDO-SE APAGA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (183, 1, N'ENCENDIDO-SE REINICIA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (184, 1, N'EQUIPO BLOQUEADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (185, 1, N'EQUIPO CALIENTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (186, 1, N'FALLA NO DETECTADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (187, 1, N'HOUSING DANADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (188, 1, N'INFRARROJO NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (189, 1, N'JOYSTICK-NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (190, 1, N'MICROFONO-NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (192, 1, N'MICROFONO-SONIDO BAJO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (193, 1, N'NO GRABA CONTACTOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (194, 1, N'NO GUARDA  FOTOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (195, 1, N'NO LEE SIM')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (196, 1, N'NO RECONOCE MEMORY CARD')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (197, 1, N'NO VIBRA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (198, 1, N'PISTA ABIERTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (199, 1, N'PROBLEMA DE CARGA-NO CARGA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (202, 1, N'REPASO DE SOLDADURAS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (203, 1, N'RINGER-NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (204, 1, N'RINGER-SONIDO BAJO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (205, 1, N'SEÑAL BAJA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (206, 1, N'TECLADO-NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (207, 1, N'TECLAS LATERALES-NO FUNCIONAN')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (208, 1, N'TRANSMISION-FALLA LLAMADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (209, 1, N'TRANSMISION-RUIDOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (211, 1, N'PROBLEMAS DE TOUCH SCREEN')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (212, 1, N'NO ENCIENDE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (213, 1, N'SE FRIZA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (214, 1, N'NO DOA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (215, 1, N'NO DAP')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (216, 1, N'BLUETOOTH NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (217, 1, N'FLEX AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (218, 1, N'NO CONECTA A LA PC')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (219, 1, N'WI FI NO CONECTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (220, 1, N'BACK COVER NO AJUSTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (221, 1, N'TRANSMISOR DE RADIO AVERIDADO')
GO
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (222, 1, N'P/A AVERIADO SE CALIENTA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (223, 1, N'NO FUNCIONA VIBRADOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (224, 1, N'EQUIPO 72/30 GRAY MARKET')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (225, 1, N'EQUIPO 72/30 FABRICANTE ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (226, 1, N'NO SUBE EL SISTEMA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (227, 1, N'NO FUNCIONA NFC')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (228, 1, N'NO NAVEGA INTERNET')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (229, 1, N'NO NAVEGA INTERNTE (EN 3G)')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (230, 1, N'NO NAVEGA INTERNET (EN 4G)')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (231, 1, N'NO NAVEGA INTERNET (CON WIFI)')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (233, 1, N'NO FUNCIONA RINGER (RUIDO)')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (234, 1, N'NO FUNCIONA AURICULAR (RUIDO)')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (235, 1, N'NO FUNCIONA CAMARA DELANTERA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (237, 1, N'NO FUNCIONA SENSOR (DE LUZ)')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (238, 1, N'NO FUNCIONA ACELEROMETRO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (239, 1, N'NO FUNCIONA GPS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (240, 1, N'NO FUNCIONA APLICACIONES ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (245, 1, N'NO FUNCIONA DISPLAY (EXTERNO)')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (248, 1, N'CAMBIAR TAPA SUPERIOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (249, 1, N'CAMBIAR TAPA FRONTAL')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (250, 1, N'EQUIPO OXIDADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (251, 1, N'TAPA INFERIOR ABOLLADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (252, 1, N'EQUIPO EN CORTO CIRCUITO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (254, 1, N'EQUIPO REINICIA SOLO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (255, 1, N'No Configuración')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (256, 1, N'NO DISPLAY LED FRONTAL')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (257, 1, N'NO POWER')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (258, 1, N'SE QUEDA CARGANDO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (259, 1, N'AUDIO VIDEO AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (260, 1, N'EQUIPO CON INTERFERENCIA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (261, 1, N'IMAGEN CON PIXELACION ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (262, 1, N'LNB IN ROTO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (263, 1, N'LNB IN TORCIDO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (264, 1, N'MALTRATO FÍSICO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (265, 1, N'NO AUDIO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (266, 1, N'NO SEÑAL')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (267, 1, N'No Video')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (268, 1, N'Quemaduras Físicas')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (269, 1, N'NO VIDEO-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (270, 1, N'NO AUDIO-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (271, 1, N'NO SENAL-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (272, 1, N'NO CONTROL REMOTO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (273, 1, N'SE REINICIA-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (274, 1, N'SE FRIZA-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (275, 1, N'NO TARJETA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (276, 1, N'HUMEDAD ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (277, 1, N'NO VERIFICA FALLA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (278, 1, N'NO CONECTA EQUIPO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (279, 1, N'NO CONECTA A LA RED ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (280, 1, N'NO RESET ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (281, 1, N'NO INICIA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (282, 1, N'NO VOZ IP ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (283, 1, N'NO WIFI')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (284, 1, N'REQUIERE MANTENIMIENTO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (286, 1, N'PROBLEMA EN TARJETA PRINCIPAL')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (287, 1, N'SOPORTES DE MOTOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (288, 1, N'PROBLEMA EN TARJETA POWER')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (289, 1, N'MECANISMO OPTICO DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (290, 1, N'MECANISMO OPTICO DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (291, 1, N'TARJETA PRINCIPAL DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (292, 1, N'MEMBRANA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (293, 1, N'INSTALACION BASICA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (294, 1, N'PROBLEMAS DE CONFIGURACION')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (295, 1, N'VALVULA SELENOIDE DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (296, 1, N'TURBINA ROTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (297, 1, N'BOTON DE DESAGUE DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (298, 1, N'DESAGUE DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (299, 1, N'MOTOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (300, 1, N'SENSOR DE PRESION DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (301, 1, N'DUCTO OBSTRUIDO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (302, 1, N'MAQUINA DE HIELO DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (303, 1, N'MAGNETRON DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (304, 1, N'MODULO WIFI DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (305, 1, N'PROBLEMA EN TARJETA INVERTER')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (306, 1, N'SERPENTIN DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (307, 1, N'DAMPER DEFECTUOSOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (308, 1, N'PROBLEMAS EN EL AGITADOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (309, 1, N'BOCINAS DEFECTUOSAS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (310, 1, N'TARJETA T-CON DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (311, 1, N'FAN DEL EVAPORADOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (312, 1, N'NO ENCIENDE ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (313, 1, N'SOLICITA INSTALACION')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (314, 1, N'CAPACITOR DE MOTOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (315, 1, N'NO ENCIENDE ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (317, 1, N'PUERTO LAN ENCENDIDO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (318, 1, N'SE CONGELA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (319, 1, N'TECLADO NO FUNCIONA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (320, 1, N'AUDIO NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (321, 1, N'SIN IMAGEN')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (322, 1, N'NO FUNCIONA MOUSE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (323, 1, N'NO RECONOCE CD')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (324, 1, N'TURBINA FLOJA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (325, 1, N'VALIDACION LICENSIA DE WINDOWS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (326, 1, N'DESINSTALACION DE AIRE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (327, 1, N'BOMBA DE AGUA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (328, 1, N'PROBLEMAS DE SOFTWARE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (330, 1, N'BOTON DE VOLUMEN DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (331, 1, N'NO ENCIENDE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (332, 1, N'CAMBIO DE FUENTE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (333, 1, N'CAMBIO MAINBOARD ')
GO
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (334, 1, N'CAMBIO DE CABLE DE CORRIENTE ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (335, 1, N'CAMBIO ADAPTADOR DE DC')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (336, 1, N'AJUSTE DE CONECTOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (337, 1, N'CAMBIO DE PANTALLA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (338, 1, N'IMAGEN BORROSA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (339, 1, N'FALLA DE COLORES ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (340, 1, N'PANTALLA NEGRA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (342, 1, N'ERROR DE BALANCE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (343, 1, N'NO CALIENTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (345, 1, N'NO ACTUALIZA SISTEMA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (346, 1, N'PIXEL LCD DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (347, 1, N'NO CONECTA WIFI')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (348, 1, N'LINEAS EN LCD')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (349, 1, N'DISCO DURO DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (350, 1, N'LCD IMAGEN OSCURA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (351, 1, N'TARJETA FRONTAL DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (352, 1, N'SUSPENSION DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (355, 1, N'BANDEJA DE CD DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (357, 1, N'PANTALLA/PANEL ROTO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (358, 1, N'PDP P-LOGIC MAIN DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (359, 1, N'PDP P-Y-MAIN BOARD DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (360, 1, N'PDP P-Y PRINCIPAL DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (363, 1, N'MOTOR DE BANDEJA DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (366, 1, N'EVAPORADOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (367, 1, N'TARJETA AMP DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (368, 1, N'TERMISTOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (369, 1, N'CAPILAR OBSTRUIDO-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (370, 1, N'BATERIA DAÑADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (373, 1, N'HOLDER ASSEMBLY DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (374, 1, N'ERROR DE COMUNICACION')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (375, 1, N'BANDEJA DE DRENAJE DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (376, 1, N'TARJETA DE DISPLAY DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (378, 1, N'TARJETA SUB DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (380, 1, N'BOMBA DE DESAGUE DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (381, 1, N'RAMAL DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (382, 1, N'PANEL DE CONTROL DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (383, 1, N'PLATE CONTROL DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (384, 1, N'BOTON DE ENCENDIDO DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (385, 1, N'TINA ROTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (386, 1, N'REJILLA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (390, 1, N'CAPACITOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (391, 1, N'VALVULA DE GAS DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (392, 1, N'BISAGRA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (393, 1, N'IMAGEN FRIZADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (394, 1, N'PUERTO HDMI DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (395, 1, N'PDP P-X-MAIN BOARD DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (396, 1, N'MANGUERA OBSTRUIDA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (397, 1, N'AGUJA OBSTRUIDA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (398, 1, N'FUSIBLE DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (399, 1, N'TRANSFORMADOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (402, 1, N'PDP P-Z-MAIN BOARD DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (403, 1, N'SISTEMA INESTABLE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (405, 1, N'MOTOR SWING DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (406, 1, N'MOTOR DE SECADO DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (407, 1, N'CAMBIO POR SERVICIO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (408, 1, N'SENSOR MAGNETICO DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (410, 1, N'CLIP DRAIN DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (411, 1, N'GROMMET-DRAIN DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (412, 1, N'CONDENSADOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (413, 1, N'CORREA DAÑADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (414, 1, N'RIELES DEFECTUOSOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (415, 1, N'PUERTAS DESCOLORIDAS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (416, 1, N'CAMBIAR HOUSING Y CAPACITOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (417, 1, N'CAMBIO CAPACITOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (418, 1, N'POWER SUPPLY DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (420, 1, N'MODULO DE USB DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (421, 1, N'TRANCISTORES DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (422, 1, N'LAMPARA LED DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (423, 1, N'PROBLEMA DE SOLDADURA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (425, 1, N'VALVULA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (426, 1, N'ERROR DC')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (427, 1, N'EQUIPO EN MODO DE PROTECCION')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (428, 1, N'FAN AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (429, 1, N'BOMBA TRANCADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (430, 1, N'ESCAPE INTERNO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (431, 1, N'ERROR 4-C')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (432, 1, N'DISPENSADOR AVERIADO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (433, 1, N'ERROR EN EL DISPLAY')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (434, 1, N'SENSOR DE THERMOBLOCK AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (435, 1, N'SPRING ETC-TENSION DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (437, 1, N'JUNTA DE PUERTA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (438, 1, N'INSULADOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (439, 1, N'PROBLEMAS EN EL RECEPTOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (440, 1, N'TANQUE DE AGUA DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (441, 1, N'TOUCH  DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (442, 1, N'RUIDO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (443, 1, N'TARJETA PIRNCIPAN DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (444, 1, N'TARJETA FUENTE DEFECTUOSA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (445, 1, N'TARJETA FRONTAL DEFECTUOSA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (446, 1, N'MOTOR EVAPORADOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (447, 1, N'NO FUNCIONA CAMARA TRASERA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (448, 1, N'PANEL DEFECTUOSO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (449, 1, N'PARLENTE DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (450, 1, N'BOCINA DEFECTUOSA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (452, 1, N'PALANCA ROTA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (453, 1, N'FUENTE DAÑADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (454, 1, N'FILTRO OBSTRUIDO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (455, 1, N'PROBLEMA DE DRENAJE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (456, 1, N'VALVULA DE SERVICIO DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (457, 1, N'CAPACITOR DEFECTUOSO')
GO
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (458, 1, N'DISPLAY AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (461, 1, N'FUELLE AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (462, 1, N'GABINETE DE FREEZER DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (464, 1, N'BOTON NO RESPONDE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (465, 1, N'SOPORTES DEFECTUOSOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (466, 1, N'TARJETA PRINCIPAL DEFECTUOSA-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (467, 1, N'QUEMADORES DEFECTUOSOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (468, 1, N'MOTOR DEFECTUOSO-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (470, 1, N'CLOCHE DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (472, 1, N'NO CALIENTA-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (473, 1, N'TIMER DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (474, 1, N'MANGUERA FLOJA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (475, 1, N'OVERLOAD  DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (476, 1, N'BOTON DE HORNO DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (477, 1, N'PROBLEMA ELECTRICO-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (478, 1, N'BOTON DE ENCENDIDO DAÑADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (479, 1, N'MODO DE PROTECCION ACTIVADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (480, 1, N'REQUIERE REFRIGERANTE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (482, 1, N'INSPECCION DE EQUIPO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (483, 1, N'PROBLEMA DE PUERTA-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (484, 1, N'REVISIÓN TECNICA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (485, 1, N'TARJETA POWER DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (486, 1, N'ERROR P4')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (487, 1, N'COMPRESOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (490, 1, N'CAPILAR OBSTRUIDO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (491, 1, N'RELAY DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (492, 1, N'TARJETA DE POTENCIA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (493, 1, N'FRECUENCIA DE BANDA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (494, 1, N'GOLPETEO DE TAMBOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (495, 1, N'BOCINA DEFECTUOSA-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (496, 1, N'BOMBA DE DRENAJE ATASCADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (497, 1, N'BASE DE CONTROL DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (499, 1, N'VALVULA DEFECTUOSA-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (500, 1, N'BOTON DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (501, 1, N'SENSOR DE PUERTA DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (502, 1, N'INSTALACION DE LAVADORA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (503, 1, N'MODO DE PROCTECCION ACTIVADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (504, 1, N'MOTOR BLOWER DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (505, 1, N'PANEL FRONTAL AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (506, 1, N'PESTILLO DE PUERTA DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (507, 1, N'TORNILLO FRONTAL DESAJUSTADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (508, 1, N'PUERTA DEL HORNO DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (509, 1, N'ESPALDAR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (510, 1, N'LIMPIEZA DE SISTEMA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (511, 1, N'PARLANTE DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (512, 1, N'REQUIERE INSTALACION')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (513, 1, N'INSTALACION DE SECADORA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (514, 1, N'PANEL PCB DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (515, 1, N'DRENAJE OBSTRUIDO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (516, 1, N'RESISTENCIA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (518, 1, N'REQUIERE MOVIMIENTO DE SENSOR')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (519, 1, N'SPIN TIMER DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (520, 1, N'BOCINAS AVERIADAS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (521, 1, N'REQUIERE INSTALACION-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (522, 1, N'FIRTRO TAPADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (523, 1, N'KIT PANEL DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (524, 1, N'TORNILLO DE MOTOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (525, 1, N'REQUIERE PROGRAMACION')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (527, 1, N'FILTRO DE CORRIENTE DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (528, 1, N'MENBRANA DE FECTUOSA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (529, 1, N'TINA DE LAVADO DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (531, 1, N'TERMOSTATO MAL UBICADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (532, 1, N'KIT DE TUBERIAS DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (533, 1, N'CABLE LVS DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (534, 1, N'TARJETA SMART DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (535, 1, N'SONIDO DISTORCIONADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (536, 1, N'ERROR F6')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (537, 1, N'FUENTE DE PODER DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (538, 1, N'NO ENFRIA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (539, 1, N'PUERTA OXIDADA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (540, 1, N'SE CONDENSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (541, 1, N'SE CONGELA EVAPORADOR  ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (542, 1, N'NO CONGELA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (543, 1, N'CAPILAR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (544, 1, N'TINA CON CORROSION')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (545, 1, N'TAPA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (546, 1, N'SOLICITA DESINSTALACION ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (547, 1, N'REQUIERE CAMBIO DE FILTRO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (548, 1, N'VALVULA DE GAS NO FUNCIONA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (550, 1, N'BLUETOOTH DEFECTUOSO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (551, 1, N'MODULO DE BLUETOOTH DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (552, 1, N'BELLOW DEFECTUOSO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (553, 1, N'RESORTE DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (554, 1, N'TERMOFUSIBLE AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (555, 1, N'TERMO DISCO AVERIADO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (556, 1, N'INSPECION DE EQUIPO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (557, 1, N'TINA OXIDADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (558, 1, N'ENCENDEDORA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (559, 1, N'IR PCB DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (560, 1, N'CABLE DE DATO DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (561, 1, N'SWITCH DE PUERTA DESCONECTADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (563, 1, N'FRONT PANEL DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (564, 1, N'TUBERIA AVERIADA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (565, 1, N'OBSTRUCCION EN EL TAMBOR ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (566, 1, N'TAPON DE DRENAJE AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (567, 1, N'AJUSTE DE DISPADOR ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (568, 1, N'ENTRENAMIENTO AL CLIENTE ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (569, 1, N'RELAY ATASCADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (570, 1, N'PROPELA DEL FAN ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (571, 1, N'ENTRENAMIENTO AL CLIENTE-')
GO
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (573, 1, N'PLOT DESCONECTADO DEL ABANICO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (574, 1, N'MORTIGUADORES DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (575, 1, N'CONDENSADOR DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (576, 1, N'REACTOR AVERIADO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (577, 1, N'CORREA DE TIEMPO AVERIADA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (579, 1, N'MODULO BLUTOOH NO FUNCIONA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (580, 1, N'TUBO CAPILAR DEFECTUOSO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (581, 1, N'CAMBIO DE BOMBA DE DRENAJE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (582, 1, N'TARJETA PRINCIPAL DE FECTUOSA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (583, 1, N'TEMPORIZADOR DESAJUSTADO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (584, 1, N'MANGUERA DE DRENAJE AVERIDA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (585, 1, N'CABLE ELECRITO DEFECTUOSO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (586, 1, N'TENSOR DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (587, 1, N'GOMAS DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (588, 1, N'TERMISTOR DEFECTUOSO-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (589, 1, N'TERMISTOR/EXIT PIPE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (590, 1, N'ENSABLE FRANCE AVERIDAD ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (591, 1, N'LED DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (592, 1, N'BOMBA DE DRENAJE DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (593, 1, N'NO ENFRIA-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (594, 1, N'FAN DEL FREZER DEFECTUOSO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (595, 1, N'BOMBA DE AGUA DESAJUSTADA ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (596, 1, N'TERMOSTATO  DEFECTUOSO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (597, 1, N'CONECTOR POWER AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (598, 1, N'HPNA ROTO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (599, 1, N'LED FIJOS')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (600, 1, N'NO HPNA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (601, 1, N'PUERTO MALO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (602, 1, N'Power Rojo ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (603, 1, N'X ROJA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (604, 1, N'NO AUDIO HDMI')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (605, 1, N'NO AUDIO RF')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (606, 1, N'NO GRABACIÓN')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (607, 1, N'NO IMAGEN')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (608, 1, N'NO VIDEO RF')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (609, 1, N'NO VIDEO HDMI')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (610, 1, N'NO VIDEO RCA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (611, 1, N'PUSH BUTTON AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (612, 1, N'EMPAQUE EQUIPOS  Y ACCESORIO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (613, 1, N'SMARTCARD DENTRO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (614, 1, N'EQUIPO SIN CABLE')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (615, 1, N'EQUIPO SE FRIZA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (616, 1, N'NO LEE SMARTCARD')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (618, 1, N'FUSIBLE AVERIADO ')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (621, 1, N'NO LEE SMARTCARD')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (622, 1, N'NO AUDIO RCA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (624, 1, N'COMPRESOR DEFECTUOSO-')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (625, 1, N'TARJETA DEFECTUOSA')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (626, 1, N'INITOR AVERIADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (627, 1, N'MOTOR FAN ATASCADO')
INSERT [dbo].[diagnosticos] ([diagnostico_id], [activo], [descripcion_diagnostico]) VALUES (628, 1, N'CORREA DEFECTUOSA')
SET IDENTITY_INSERT [dbo].[diagnosticos] OFF
SET IDENTITY_INSERT [dbo].[estado_equipo] ON 

INSERT [dbo].[estado_equipo] ([estado_equipo_id], [activo], [descripcion]) VALUES (1, 1, N'Ingreso')
INSERT [dbo].[estado_equipo] ([estado_equipo_id], [activo], [descripcion]) VALUES (2, 1, N'dianostico')
INSERT [dbo].[estado_equipo] ([estado_equipo_id], [activo], [descripcion]) VALUES (3, 1, N'reparando')
INSERT [dbo].[estado_equipo] ([estado_equipo_id], [activo], [descripcion]) VALUES (4, 1, N'Despachado')
INSERT [dbo].[estado_equipo] ([estado_equipo_id], [activo], [descripcion]) VALUES (5, 1, N'Equipo Recepcionado')
SET IDENTITY_INSERT [dbo].[estado_equipo] OFF
SET IDENTITY_INSERT [dbo].[estado_ubicacion] ON 

INSERT [dbo].[estado_ubicacion] ([estado_ubicacion_id], [activo], [descripcion]) VALUES (1, 1, N'home')
INSERT [dbo].[estado_ubicacion] ([estado_ubicacion_id], [activo], [descripcion]) VALUES (2, 1, N'Taller')
INSERT [dbo].[estado_ubicacion] ([estado_ubicacion_id], [activo], [descripcion]) VALUES (3, 1, N'almacen')
SET IDENTITY_INSERT [dbo].[estado_ubicacion] OFF
SET IDENTITY_INSERT [dbo].[falla] ON 

INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (1, 1, N'No enciende')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (2, 1, N'No prende')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (3, 1, N'Tarjeta danada')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (4, 1, N'motor averiado')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (6, 1, N'led averiado')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (7, 1, N'no cogela')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (8, 1, N'Humedad')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (10, 1, N'alamtre sufatado ')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (11, 1, N'Boartd no prende')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (14, 1, N'No apaga')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (15, 1, N'se Desconfigura')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (16, 1, N'boton hundido')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (17, 1, N'transitor averiafdo')
INSERT [dbo].[falla] ([falla_id], [activo], [descripcion_falla]) VALUES (18, 1, N'transmidor averiado')
SET IDENTITY_INSERT [dbo].[falla] OFF
SET IDENTITY_INSERT [dbo].[garantia] ON 

INSERT [dbo].[garantia] ([garantia_id], [activo], [meses_gatia]) VALUES (1, 1, 90)
INSERT [dbo].[garantia] ([garantia_id], [activo], [meses_gatia]) VALUES (2, 1, 120)
SET IDENTITY_INSERT [dbo].[garantia] OFF
SET IDENTITY_INSERT [dbo].[marca] ON 

INSERT [dbo].[marca] ([marca_id], [activo], [nombre_marca], [descripcion], [fecha_ingreso]) VALUES (1, 1, N'samsung', N'samsung', CAST(0x0000A7D9003446D0 AS DateTime))
INSERT [dbo].[marca] ([marca_id], [activo], [nombre_marca], [descripcion], [fecha_ingreso]) VALUES (2, 1, N'LG', N'LG', CAST(0x0000A7D9003446D0 AS DateTime))
INSERT [dbo].[marca] ([marca_id], [activo], [nombre_marca], [descripcion], [fecha_ingreso]) VALUES (3, 1, N'westinghouse', N'westinghouse', CAST(0x0000A7D9003446D0 AS DateTime))
INSERT [dbo].[marca] ([marca_id], [activo], [nombre_marca], [descripcion], [fecha_ingreso]) VALUES (4, 1, N'Daewoo', N'Daewo', CAST(0x0000A7D9003446D0 AS DateTime))
SET IDENTITY_INSERT [dbo].[marca] OFF
SET IDENTITY_INSERT [dbo].[modelo] ON 

INSERT [dbo].[modelo] ([modelo_id], [activo], [nombre_modelo], [descripcion], [fecha_ingreso], [tipo_producto_id]) VALUES (3, 1, N'wqbhtu', N'', CAST(0x0000A7DB005DEEE0 AS DateTime), 2)
INSERT [dbo].[modelo] ([modelo_id], [activo], [nombre_modelo], [descripcion], [fecha_ingreso], [tipo_producto_id]) VALUES (4, 1, N'mopqdrty', N'', CAST(0x0000A7DB005DEEE0 AS DateTime), 1)
INSERT [dbo].[modelo] ([modelo_id], [activo], [nombre_modelo], [descripcion], [fecha_ingreso], [tipo_producto_id]) VALUES (5, 1, N'flex', N'', CAST(0x0000A7DB015BA288 AS DateTime), 1)
INSERT [dbo].[modelo] ([modelo_id], [activo], [nombre_modelo], [descripcion], [fecha_ingreso], [tipo_producto_id]) VALUES (6, 1, N'habana', N'', CAST(0x0000A7DB015BA288 AS DateTime), 2)
INSERT [dbo].[modelo] ([modelo_id], [activo], [nombre_modelo], [descripcion], [fecha_ingreso], [tipo_producto_id]) VALUES (7, 1, N'cuvumbu', N'', CAST(0x0000A7DB015BA288 AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[modelo] OFF
SET IDENTITY_INSERT [dbo].[ordenOperacion] ON 

INSERT [dbo].[ordenOperacion] ([orden_id], [activo], [marca_id], [modelo_id], [cliente_id], [serie], [falla_id], [fecha_fabricante], [fecha_venta], [tipo_producto_id], [garantia_id], [estado_equipo_id], [estado_ubicacion_id]) VALUES (7, 1, 2, 3, 1, N'werqtbnk', 11, CAST(0x0000A7DB005D7C08 AS DateTime), CAST(0x0000A7DB005D7C08 AS DateTime), 3, 2, 2, 2)
INSERT [dbo].[ordenOperacion] ([orden_id], [activo], [marca_id], [modelo_id], [cliente_id], [serie], [falla_id], [fecha_fabricante], [fecha_venta], [tipo_producto_id], [garantia_id], [estado_equipo_id], [estado_ubicacion_id]) VALUES (10, 1, 1, 3, 1, N'werqtbnk', 11, CAST(0x0000A7DB005D7C08 AS DateTime), CAST(0x0000A7DB005D7C08 AS DateTime), 3, 2, 2, 2)
INSERT [dbo].[ordenOperacion] ([orden_id], [activo], [marca_id], [modelo_id], [cliente_id], [serie], [falla_id], [fecha_fabricante], [fecha_venta], [tipo_producto_id], [garantia_id], [estado_equipo_id], [estado_ubicacion_id]) VALUES (11, 1, 1, 4, 1, N'werqtbnk', 11, CAST(0x0000A7DB005D7C08 AS DateTime), CAST(0x0000A7DB005D7C08 AS DateTime), 3, 2, 2, 2)
SET IDENTITY_INSERT [dbo].[ordenOperacion] OFF
SET IDENTITY_INSERT [dbo].[partes] ON 

INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (1, 1, N'led')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (2, 1, N'manguera')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (3, 1, N'Sintonizador')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (4, 1, N'Microcontrolador')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (5, 1, N'Jungla')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (6, 1, N'Audio')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (7, 1, N'Vertical')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (8, 1, N'Horizontal')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (9, 1, N'Yugo')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (10, 1, N'Bocina')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (11, 1, N'Motor eléctrico')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (12, 1, N'Microprocesador')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (13, 1, N'Temporizador')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (14, 1, N'Tambor')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (15, 1, N'Carter del tambor')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (16, 1, N'Amortiguadores')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (17, 1, N'Bisagras para puertas')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (18, 1, N'Condensadore')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (19, 1, N'Contrapeso')
INSERT [dbo].[partes] ([parte_id], [activo], [Descripcion_parte]) VALUES (20, 1, N'Correa')
SET IDENTITY_INSERT [dbo].[partes] OFF
SET IDENTITY_INSERT [dbo].[reparacion] ON 

INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (1, 1, N'NO REPARADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (2, 1, N'NO AJUSTADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (3, 1, N'FALLA NO DETECTADA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (4, 1, N'ACTUALIZACION DE SOFTWARE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (5, 1, N'CAMBIO DE BOARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (6, 1, N'CAMBIO DE CAMARA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (7, 1, N'CAMBIO DE CONECTOR DE CARGA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (8, 1, N'CAMBIO DE CONECTOR DE SIMCARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (9, 1, N'CAMBIO DE DISPLAY')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (10, 1, N'CAMBIO DE FLEX')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (11, 1, N'CAMBIO DE FLEX DE PANTALLA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (12, 1, N'CAMBIO DE FLEX DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (13, 1, N'CAMBIO DE FLEX DE VOLUMEN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (14, 1, N'CAMBIO DE HOUSING')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (15, 1, N'CAMBIO DE IC DE CARGA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (16, 1, N'CAMBIO DE IC DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (17, 1, N'CAMBIO DE IC DE USB')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (18, 1, N'CAMBIO DE JOYSTICK')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (19, 1, N'CAMBIO DE MICROFONO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (20, 1, N'CAMBIO DE MOTOR VIBRADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (21, 1, N'CAMBIO DE PASADOR DE FLIP')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (22, 1, N'CAMBIO DE PIN DE ANTENA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (23, 1, N'CAMBIO DE RINGER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (24, 1, N'CAMBIO DE SPEAKER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (25, 1, N'CAMBIO DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (26, 1, N'CAMBIO TARJETA DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (27, 1, N'LIMPIEZA DE EQUIPO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (28, 1, N'CAMBIO DE TOUCH SCREEN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (29, 1, N'CAMBIO DE BATERIA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (30, 1, N'CAMBIO DE IC ENCENDIDO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (31, 1, N'CAMBIO DE PA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (32, 1, N'CAMBIO DE CARGADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (33, 1, N'AJUSTE DE ENCENDIDO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (34, 1, N'AJUSTE DE RED')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (35, 1, N'AJUSTE DE P.A.')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (36, 1, N'AJUSTE CONECTOR DE SIM CARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (37, 1, N'AJUSTE IC DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (38, 1, N'AJUSTE IC DE CARGA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (39, 1, N'AJUSTE DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (40, 1, N'CAMBIO FLEX DE SPEAKER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (41, 1, N'AJUSTE PARAMETROS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (42, 1, N'CAMBIO DE  HAND FREE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (43, 1, N'CAMBIO DE AMP. DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (44, 1, N'CAMBIO DE BACK COVER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (45, 1, N'CAMBIO DE AURICULAR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (47, 1, N'CAMBIO EFECTUADO GRAY MARKET')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (49, 1, N'CAMBIO DE EQUIPO EN GARANTIA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (51, 1, N'CAMBIO DE COMPRESOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (52, 1, N'CAMBIO DE DISPLAY')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (53, 1, N'CAMBIO DE TARJETA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (54, 1, N'CARGA DE GAS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (55, 1, N'REPARACION D CONTROL REMOTO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (56, 1, N'REPARACION ELECTRICA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (57, 1, N'CAMBIO DE BOMBA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (58, 1, N'CAMBIO DE DISPLAY')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (59, 1, N'CAMBIO DE MOTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (60, 1, N'CAMBIO DE TAPA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (61, 1, N'REPARACION DE BOMBA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (62, 1, N'REPARACION DE DRENAJE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (63, 1, N'REPARACION ELECTRICA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (64, 1, N'CAMBIO DE ABANICO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (65, 1, N'CAMBIO DE BOMBILLO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (66, 1, N'CAMBIO DE FUENTE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (67, 1, N'CAMBIO DE PANEL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (68, 1, N'CAMBIO DE PLATO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (69, 1, N'REPARACION ELECTRICA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (70, 1, N'CAMBIO DE LECTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (71, 1, N'CAMBIO DE TARJETA DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (72, 1, N'REPARACION TARJETA DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (73, 1, N'CAMBIO DE ABANICO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (74, 1, N'CAMBIO DE EMPAQUE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (75, 1, N'CAMBIO DE FILTRO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (76, 1, N'CAMBIO DE FUSIBLE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (77, 1, N'CAMBIO DE COMPRESOR/MOTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (78, 1, N'CAMBIO DE OVERLOAD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (79, 1, N'CAMBIO DE RELAY')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (80, 1, N'CAMBIO DE TARJETA DE PANEL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (81, 1, N'CAMBIO DE TERMOSTATO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (82, 1, N'CAMBIO DE TIMER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (83, 1, N'CAMBIO DE VISAGRAS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (84, 1, N'CARGA DE GAS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (85, 1, N'AJUSTE DE DRENAJE-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (86, 1, N'REPARACION ELECTRICA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (87, 1, N'CAMBIO DE LECTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (88, 1, N'CAMBIO DE TARJETA DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (89, 1, N'REPARACION TARJETA DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (90, 1, N'AJUSTE DE FUNCIONES')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (91, 1, N'AJUSTE DE PANTALLA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (92, 1, N'CAMBIO DE CABLE DE CORRIENTE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (93, 1, N'CAMBIO DE FILTROS?')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (94, 1, N'CAMBIO DE FUSIBLE?')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (95, 1, N'CAMBIO DE PANTALLA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (96, 1, N'CAMBIO DE PIEZA MODULO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (97, 1, N'CAMBIO DE TARJETA POWER SUPPLY')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (98, 1, N'CAMBIO DE TARJETA DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (99, 1, N'CAMBIO DE TARJETA DE VIDEO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (100, 1, N'CAMBIO DE TARJETA LOGICA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (101, 1, N'LIMPIEZA DE TARJETA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (102, 1, N'REPARACION TARJETA DE AUDIO')
GO
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (103, 1, N'REPARACION TARJETA DE VIDEO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (104, 1, N'REPARACION TARJETA LOGICA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (105, 1, N'Corregir voltaje')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (106, 1, N'Cambiar resistencia')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (107, 1, N'Cambiar o reparar cable')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (108, 1, N'Cambiar bimetálico')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (109, 1, N'Cambiar plancha')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (110, 1, N'Cambiar bombillo ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (111, 1, N'Corregir corto')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (112, 1, N'Cambio de accesorio o manubio ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (113, 1, N'CAMBIO CONECTOR DE HANDS FREE ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (114, 1, N'CAMBIO  DE IC  DE  CAMARA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (115, 1, N'CAMBIO DE ANTENA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (116, 1, N'CAMBIO DE MAYLER DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (117, 1, N'CAMBIO DE MAILER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (118, 1, N'CAMBIO DE FRONT HOUSING')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (119, 1, N'CAMBIO DE CABLE COAXIAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (120, 1, N'CAMBIO DE LINTERNA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (121, 1, N'CAMBIO CONECTOR DE MEMORIA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (122, 1, N'CAMBIO CONECTOR DE BATERÍA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (123, 1, N'CABLE LÍNEA TELEFÓNICA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (124, 1, N'CAMBIO DE IC DE BLUETOOTH')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (125, 1, N'NO REPARADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (126, 1, N'NO AJUSTADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (127, 1, N'ACTUALIZACION DE SOFTWARE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (128, 1, N'CAMBIO DE BOARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (129, 1, N'CAMBIO DE CAMARA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (130, 1, N'CAMBIO DE CONECTOR DE CARGA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (131, 1, N'CAMBIO DE CONECTOR DE SIMCARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (132, 1, N'CAMBIO DE DISPLAY')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (133, 1, N'CAMBIO DE FLEX')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (134, 1, N'CAMBIO DE FLEX DE PANTALLA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (135, 1, N'CAMBIO DE FLEX DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (136, 1, N'CAMBIO DE FLEX DE VOLUMEN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (137, 1, N'CAMBIO DE HOUSING')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (138, 1, N'CAMBIO DE IC DE CARGA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (139, 1, N'CAMBIO DE IC DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (140, 1, N'CAMBIO DE IC DE USB')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (141, 1, N'CAMBIO DE JOYSTICK')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (142, 1, N'CAMBIO DE MICROFONO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (143, 1, N'CAMBIO DE MOTOR VIBRADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (144, 1, N'CAMBIO DE PASADOR DE FLIP')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (145, 1, N'CAMBIO DE PIN DE ANTENA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (146, 1, N'CAMBIO DE RINGER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (147, 1, N'CAMBIO DE SPEAKER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (148, 1, N'CAMBIO DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (149, 1, N'CAMBIO TARJETA DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (150, 1, N'LIMPIEZA DE EQUIPO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (151, 1, N'CAMBIO DE TOUCH SCREEN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (152, 1, N'CAMBIO DE BATERIA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (153, 1, N'CAMBIO DE IC ENCENDIDO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (154, 1, N'CAMBIO DE PA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (155, 1, N'CAMBIO DE CARGADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (156, 1, N'AJUSTE DE ENCENDIDO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (157, 1, N'AJUSTE DE RED')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (158, 1, N'AJUSTE DE P.A.')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (159, 1, N'AJUSTE CONECTOR DE SIM CARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (160, 1, N'AJUSTE IC DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (161, 1, N'AJUSTE IC DE CARGA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (162, 1, N'AJUSTE DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (163, 1, N'CAMBIO FLEX DE SPEAKER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (164, 1, N'AJUSTE PARAMETROS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (165, 1, N'CAMBIO DE  HAND FREE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (166, 1, N'CAMBIO DE AMP. DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (167, 1, N'CAMBIO DE BACK COVER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (168, 1, N'CAMBIO DE AURICULAR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (170, 1, N'CAMBIO EFECTUADO GRAY MARKET')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (172, 1, N'CAMBIO DE EQUIPO EN GARANTIA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (174, 1, N'CAMBIO CONECTOR DE HANDS FREE ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (175, 1, N'CAMBIO  DE IC  DE  CAMARA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (176, 1, N'CAMBIO DE ANTENA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (177, 1, N'CAMBIO DE MAYLER DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (178, 1, N'CAMBIO DE MAILER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (179, 1, N'CAMBIO DE FRONT HOUSING')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (180, 1, N'CAMBIO DE CABLE COAXIAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (181, 1, N'CAMBIO DE LINTERNA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (182, 1, N'CAMBIO CONECTOR DE MEMORIA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (183, 1, N'CAMBIO CONECTOR DE BATERÍA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (184, 1, N'CABLE LÍNEA TELEFÓNICA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (185, 1, N'CAMBIO DE IC DE BLUETOOTH')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (186, 1, N'NO REPARADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (187, 1, N'NO AJUSTADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (188, 1, N'ACTUALIZACION DE SOFTWARE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (189, 1, N'CAMBIO DE BOARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (190, 1, N'CAMBIO DE CAMARA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (191, 1, N'CAMBIO DE CONECTOR DE CARGA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (192, 1, N'CAMBIO DE CONECTOR DE SIMCARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (193, 1, N'CAMBIO DE DISPLAY')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (194, 1, N'CAMBIO DE FLEX')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (195, 1, N'CAMBIO DE FLEX DE PANTALLA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (196, 1, N'CAMBIO DE FLEX DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (197, 1, N'CAMBIO DE FLEX DE VOLUMEN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (198, 1, N'CAMBIO DE HOUSING')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (199, 1, N'CAMBIO DE IC DE CARGA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (200, 1, N'CAMBIO DE IC DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (201, 1, N'CAMBIO DE IC DE USB')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (202, 1, N'CAMBIO DE JOYSTICK')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (203, 1, N'CAMBIO DE MICROFONO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (204, 1, N'CAMBIO DE MOTOR VIBRADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (205, 1, N'CAMBIO DE PASADOR DE FLIP')
GO
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (206, 1, N'CAMBIO DE PIN DE ANTENA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (207, 1, N'CAMBIO DE RINGER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (208, 1, N'CAMBIO DE SPEAKER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (209, 1, N'CAMBIO DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (210, 1, N'CAMBIO TARJETA DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (211, 1, N'LIMPIEZA DE EQUIPO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (212, 1, N'CAMBIO DE TOUCH SCREEN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (213, 1, N'CAMBIO DE BATERIA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (214, 1, N'CAMBIO DE IC ENCENDIDO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (215, 1, N'CAMBIO DE PA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (216, 1, N'CAMBIO DE CARGADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (217, 1, N'AJUSTE DE ENCENDIDO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (218, 1, N'AJUSTE DE RED')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (219, 1, N'AJUSTE DE P.A.')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (220, 1, N'AJUSTE CONECTOR DE SIM CARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (221, 1, N'AJUSTE IC DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (222, 1, N'AJUSTE IC DE CARGA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (223, 1, N'AJUSTE DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (224, 1, N'CAMBIO FLEX DE SPEAKER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (225, 1, N'AJUSTE PARAMETROS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (226, 1, N'CAMBIO DE  HAND FREE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (227, 1, N'CAMBIO DE AMP. DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (228, 1, N'CAMBIO DE BACK COVER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (229, 1, N'CAMBIO DE AURICULAR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (231, 1, N'CAMBIO EFECTUADO GRAY MARKET')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (233, 1, N'CAMBIO DE EQUIPO EN GARANTIA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (235, 1, N'CAMBIO CONECTOR DE HANDS FREE ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (236, 1, N'CAMBIO  DE IC  DE  CAMARA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (237, 1, N'CAMBIO DE ANTENA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (238, 1, N'CAMBIO DE MAYLER DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (239, 1, N'CAMBIO DE MAILER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (240, 1, N'CAMBIO DE FRONT HOUSING')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (241, 1, N'CAMBIO DE CABLE COAXIAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (242, 1, N'CAMBIO DE LINTERNA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (243, 1, N'CAMBIO CONECTOR DE MEMORIA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (244, 1, N'CAMBIO CONECTOR DE BATERÍA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (245, 1, N'CABLE LÍNEA TELEFÓNICA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (246, 1, N'CAMBIO DE IC DE BLUETOOTH')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (247, 1, N'CAMBIO MODULO DE CARGA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (248, 1, N'CAMBIO IC DE SENSOR ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (249, 1, N'CAMBIO DE SWITH-TACT')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (250, 1, N'CAMBIO DE IC DE  BLUEETOOTH')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (251, 1, N'CAMBIO  DE PORTA MEMORIA SD ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (252, 1, N'CAMBIO DE TARJETA DE DISPLAY ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (253, 1, N'CAMBIO DE  ADHESIVO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (254, 1, N'CAMBIO DE SENSOR PROXIMIDAD ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (255, 1, N'AJUSTE DE ENCENDIDO-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (256, 1, N'CAMBIO DE TRANSFORMADOR ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (257, 1, N'CAMBIO DE FUSIBLE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (258, 1, N'CAMBIO DE IC VOLTAJE (U3) ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (259, 1, N'CAMBIO DE CAPACITOR 33MF')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (261, 1, N'CAMBIO DE CONECTOR LNB')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (262, 1, N'CAMBIO DE IC DE VIDEO (U14)')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (263, 1, N'CAMBIO DE CONECTOR AUDIO-VIDEO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (264, 1, N'CAMBIO DE IC DE AUDIO (U15)')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (265, 1, N'CAMBIO DE RESISTENCIA DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (267, 1, N'RESOLDAR IC SENAL (U16)')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (268, 1, N'SUSTITUCION TAPA O PINTURAS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (269, 1, N'RECONSTRUCCION DE PISTA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (270, 1, N'CAMBIO DE MEMORIA FLASH')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (271, 1, N'DISPLAY DEFECTUOSO ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (272, 1, N'CAMBIO DE PARTES COSMETICAS ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (273, 1, N'RECONSTRUCCION DE PISTA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (274, 1, N'RESOLDADURA DE IC ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (275, 1, N'CAMBIO DE DISPLAY AVERIADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (277, 1, N'AJUSTE DE VIDEO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (278, 1, N'AJUSTE DE AUDIO-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (279, 1, N'AJUSTE DE SENAL ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (280, 1, N'CAMBIO DE TARJETA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (281, 1, N'CAMBIO DE IC ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (282, 1, N'CAMBIO DE COMPONENTE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (283, 1, N'INSTALACION BASICA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (284, 1, N'MANTENIMIENTO DE EQUIPO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (285, 1, N'AJUSTE MECANICO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (286, 1, N'CAMBIO DE SWITCH DE ENCENDIDO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (287, 1, N'CAMBIO DE MODULO WIFI')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (288, 1, N'CAMBIO DE TARJETA PRINCIPAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (289, 1, N'AJUSTE DE LA EEPROM')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (290, 1, N'CAMBIO DE TURBINA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (292, 1, N'AJUSTE DE TRANSMISION')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (293, 1, N'AJUSTE DE MAQUINA DE HIELO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (294, 1, N'CAMBIO DE PUERTAS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (295, 1, N'CAMBIO DE DAMPER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (296, 1, N'CAMBIO DE MAGNETRON')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (297, 1, N'CAMBIO DE MEMBRANA DEL TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (299, 1, N'CAMBIO DE TARJETA PRINCIPAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (300, 1, N'CAMBIO DE VALVULA SELENOIDE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (301, 1, N'CAMBIO DE MAQUINA DE HIELO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (302, 1, N'CAMBIO DE DRENAJE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (303, 1, N'CARGA DE REFRIGERANTE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (304, 1, N'AJUSTE DE SISTEMA ELECTRICO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (305, 1, N'AJUSTE DE DUCTO DE SALIDA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (306, 1, N'CAMBIO DE TARJETA INVERTER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (307, 1, N'CAMBIO DE TARJETA DE CONTROL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (308, 1, N'CAMBIO DE MANGUERA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (309, 1, N'CAMBIO DE TARJETA FRONTAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (310, 1, N'AJUSTE DE DAMPER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (311, 1, N'AJUSTE DE DUCTO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (312, 1, N'CAMBIO DE SERPENTIN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (313, 1, N'CAMBIO DE AGITADOR')
GO
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (314, 1, N'CAMBIO DE BOCINAS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (315, 1, N'AJUSTE DEL FAN DEL COMPRESOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (317, 1, N'CAMBIO DE VALVULA DE EXPANSION')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (318, 1, N'ACTUALIZACION DE SOFTWARE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (319, 1, N'AJUSTE DE DISPLAY')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (320, 1, N'CAMBIO DISCO DE NIVEL ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (322, 1, N'CAMBIO DE DISCO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (323, 1, N'CAMBIO DE FUENTE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (324, 1, N'CAMBIO-DE MICROFONO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (325, 1, N'AJUSTE UNIDAD CD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (326, 1, N'CAMBIO DE UNIDAD CD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (327, 1, N'AJUSTE DE FAN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (328, 1, N'CAMBIO DE FAN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (329, 1, N'AJUSTE DE CONECTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (330, 1, N'CAMBIO DE PANTALLA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (331, 1, N'ACTUALIZACION DRIVER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (332, 1, N'CAMBIO MAINBOARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (333, 1, N'CAMBIO DE ADAPTADOR DE DC')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (334, 1, N'CAMBIO DE MEMORIA RAM')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (335, 1, N'CAMBIO DE ALTAVOZ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (336, 1, N'ACTUALIZACION DE BIOS ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (337, 1, N'AJUSTE DE TURBINA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (338, 1, N'INYECCION DE LLAVE ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (339, 1, N'DESINSTALACION DE AIRE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (340, 1, N'RESTAURACION DE SISTEMA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (341, 1, N'RESTAURACION DE SISTEMA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (342, 1, N'CAMBIO DEL FAN DEL COMPRESOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (343, 1, N'CAMBIO DEL FAN DEL EVAPORADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (344, 1, N'CAMBIO DE VALVULA DE AGUA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (346, 1, N'AJUSTE DE VALVULA DE AGUA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (347, 1, N'CAMBIO DE TRANSMISION')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (348, 1, N'RECUPERACION DE INICIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (349, 1, N'RECUPERACION DE INICIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (350, 1, N'CAMBIO DE DISPENSADOR DE AGUA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (351, 1, N'CAMBIO DE FILTRO HIDRATADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (352, 1, N'CAMBIO DE MECANISMO OPTICO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (353, 1, N'CAMBIO DE SOPORTE PLASTICO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (354, 1, N'CAMBIO DE CABLE DE CORRIENTE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (355, 1, N'CAMBIO DE RIELES')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (356, 1, N'CAMBIO BOMBA DE AGUA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (357, 1, N'AJUSTE DE BOMBA DE AGUA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (359, 1, N'CAMBIO DE SENSOR DE PRESION')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (360, 1, N'AJUSTE TARJETA WIFI')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (361, 1, N'CAMBIO TARJETA WIFI')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (362, 1, N'CAMBIO DE SUSPENSION')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (363, 1, N'CAMBIO DEL FAN DEL FREEZER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (365, 1, N'CAMBIO DE BANDEJA DE CD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (367, 1, N'CAMBIO DE CABLE EDP LVDS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (368, 1, N'CAMBIO DE CABLE EDP LVDS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (369, 1, N'CAMBIO PDP P-LOGIC MAIN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (370, 1, N'CAMBIO PDP -Y-MAIN BOARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (371, 1, N'CAMBIO PDP P-Y PRINCIPAL ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (372, 1, N'CAMBIO DE TARJETA T-CON')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (374, 1, N'AJUSTE DE BANDEJA DE CD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (377, 1, N'CAMBIO DE EVAPORADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (378, 1, N'CAMBIO DE TARJETA AMP')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (380, 1, N'CAMBIO DE TERMISTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (382, 1, N'CAMBIO DE MOTOR DE LAVADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (384, 1, N'AJUSTE DE PANEL/PANTALLA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (386, 1, N'CAMBIO DE TECLADO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (387, 1, N'CAMBIO DE HOLDER ASSEMBLY')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (388, 1, N'CAMBIO DE BANDEJA DE DRENAJE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (389, 1, N'CAMBIO DE TIRA DE LEDS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (390, 1, N'CAMBIO DE PANEL DE CONTROL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (391, 1, N'CAMBIO DE TARJETA SUB')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (392, 1, N'CAMBIO DE SPEAKER SYSTEM TOTAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (393, 1, N'CAMBIO DE BOMBA DE DESAGUE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (395, 1, N'CAMBIO DE RAMAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (396, 1, N'CAMBIO DE PLATE CONTROL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (397, 1, N'AJUSTE DE PUERTA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (398, 1, N'CAMBIO DE TINA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (399, 1, N'CAMBIO DE REJILLA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (402, 1, N'CAMBIO DE TAPE DE POWER CABLE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (403, 1, N'AJUSTE CONECTOR DE CARGA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (404, 1, N'AJUSTE CONECTOR DE CARGA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (405, 1, N'CAMBIO DE PILA DE BIOS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (406, 1, N'CAMBIO DE TOUCHPAD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (407, 1, N'CAMBIO DE CINTA CONDUCTIVA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (408, 1, N'CAMBIO DE CAPACITOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (409, 1, N'CAMBIO DE IDIOMA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (411, 1, N'AJUSTE DEL SOPORTE DEL MOTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (412, 1, N'CAMBIO DE RESORTE DE PUERTA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (413, 1, N'CAMBIO DE LOGO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (414, 1, N'CAMBIO DE DUCTO DE GAS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (415, 1, N'CAMBIO DE BISAGRA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (416, 1, N'CAMBIO DE TERMODISCO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (417, 1, N'LIMPIEZA DE PLATOS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (418, 1, N'CAMBIO DE PLATO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (419, 1, N'CAMBIO DE MANGUERA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (420, 1, N'CAMBIO DE MEDIDOR DE FLUJO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (421, 1, N'CAMBIO DE TANQUE DE AGUA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (422, 1, N'CAMBIO DE BOMBA DE AGUA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (425, 1, N'CAMBIO PDP -Z-MAIN BOARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (427, 1, N'CAMBIO DE MOTOR SWING')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (428, 1, N'REPARACION DE TARJETA X-MAIN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (429, 1, N'CAMBIO POR SERVICIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (430, 1, N'CAMBIO DE CONDENSADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (431, 1, N'CAMBIO DE CUBETA DE HIELO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (433, 1, N'CAMBIO DE CLIP DRAIN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (434, 1, N'CAMBIO DE GROMMET-DRAIN')
GO
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (435, 1, N'CAMBIO DE CORREA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (437, 1, N'CONFIGURACION DE SISTEMA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (438, 1, N'REPARACION DE POWER SUPPLY')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (439, 1, N'CAMBIO DE  CUBIERTA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (440, 1, N'INSPECCION DE EQUIPO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (441, 1, N'CAMBIO DE MOTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (442, 1, N'CAMBIO DE ANILLO DE ROTACIÓN')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (443, 1, N'CAMBIO DE TARJETA DE POTENCIA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (444, 1, N'CAMBIO DE MODULO USB')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (445, 1, N'CAMBIO DE TRANSISTORES')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (446, 1, N'CAMBIO DE CAPACITORES')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (447, 1, N'CAMBIO DE LAMPARA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (448, 1, N'AJUSTE DE SOLDADURA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (449, 1, N'CAMBIO DE LAMPARA DEL PANEL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (451, 1, N'CAMBIO DE VALVULA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (452, 1, N'MANTENIMIENTO PREVETIVO ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (453, 1, N'CAMBIO DE CINTA ADHESIVA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (454, 1, N'CORTE DE JUMPER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (455, 1, N'CAMBIO DE LEAD CONNECTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (456, 1, N'REPASO DE SOLDADURA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (457, 1, N'DESARME DE BOMBA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (458, 1, N'CAMBIO DE DISPENSADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (459, 1, N'CAMBIO DE FILTRO PARA LAVADORA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (461, 1, N'CAMBIO DE INSOLADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (462, 1, N'CAMBIO DE  POWER BOARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (463, 1, N'CAMBIO DE MAIN BD A AUO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (464, 1, N'CONTROL BOARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (465, 1, N'CAMBIO CONTROL BOARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (466, 1, N'CAMBIO DE MAIN BOARD')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (467, 1, N'CAMBIO DE SENSOR DE PRESION ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (468, 1, N'CAMBIO DE SPRING ETC-TENSION ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (470, 1, N'CAMBIO DE  BOARD PAD POGO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (472, 1, N'CAMBIO DE JUNTA DE PUERTA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (473, 1, N'CAMBIO DE INSULADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (474, 1, N'CAMBIO DE RECEPTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (475, 1, N'CAMBIO DE TAMQUE DE AGUA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (476, 1, N'CAMBIO DE MODULO BLUETOTH')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (477, 1, N'CAMBIO DE MODULO BLUETOTH')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (478, 1, N'CAMBIO DE MODULO BLUETOTH')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (482, 1, N'MANTENIMIENTO DE EQUIPO-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (483, 1, N'CAMBIO DE CARGADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (484, 1, N'CAMBIO DE CAPILAR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (485, 1, N'CAMBIO DE PARLANTE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (486, 1, N'CAMBIO DE MODULO  BLUETOOTH ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (487, 1, N'LCD COVER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (488, 1, N'AJUSTE DE DRENAJE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (489, 1, N'CAMBIO DE VALVULA DE SERVCIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (490, 1, N'AJUSTE DE TAMBOR ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (491, 1, N'SUCURSAL-UPDATE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (492, 1, N'SUCURSAL - UPDATE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (493, 1, N'CAMBIO DE BOCINA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (495, 1, N'AJUSTE ELECTRICO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (496, 1, N'CAMBIO DE MUELLE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (497, 1, N'CAMBIO DE AMORTIGUADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (498, 1, N'AJUSTE DE AUDIO--')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (499, 1, N'ENTRENAMIENTO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (500, 1, N'AJUSTE ELECTRICO-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (501, 1, N'CAMBIO DE SOPORTES')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (502, 1, N'CAMBIO DE TARJETA PRICIPAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (503, 1, N'INSPECCION DE EQUIPO--')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (506, 1, N'CAMBIO DE CLOCHE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (507, 1, N'SMART TOOL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (508, 1, N'CAMBIO DE RESISTENCIA DE CALOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (509, 1, N'ACTUALIZACION DE SOFTWARE-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (510, 1, N'FALLA NO DETECTADA-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (511, 1, N'AJUSTE DE MANGUERA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (512, 1, N'CAMBIO DE MODULO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (513, 1, N'CAMBIO RESISTENCIA METAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (514, 1, N'CAMBIO FUSIBLE TERMICO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (515, 1, N'AJUSTE DE BOTON DE HORNO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (516, 1, N'INSPECCION DE EQUIPO-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (517, 1, N'MASTER RESET')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (518, 1, N'REQUIERE REFRIGERANTE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (519, 1, N'CAMBIO DE PARRILLA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (520, 1, N'CAMBIO DE DUCTO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (521, 1, N'CAMBIO DE CORDON')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (522, 1, N'CAMBIO DE CONTROLADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (523, 1, N'REVISIÓN TECNICA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (524, 1, N'INSTALACION BASICA-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (525, 1, N'AJUSTE DE LA VALVULA DE GAS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (526, 1, N'CAMBIO DE TARJETA POWER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (527, 1, N'CAMBIO DE ESPITA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (528, 1, N'CAMBIO MAGNETO DE PUERTA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (530, 1, N'CAMBIO GOMA DE PUERTA FREZZER ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (531, 1, N'AJUSTE DE FRECUENCIA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (532, 1, N'CAMBIO DE POWER SUPPLY')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (533, 1, N'AJUSTE DE AGITADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (534, 1, N'AJUSTE DE NIVEL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (535, 1, N'AJUSTE BOMBA DE DRENAJE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (537, 1, N'CAMBIO DE BASE DE CONTROL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (538, 1, N'CAMBIO DE INTERRUPTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (539, 1, N'CAMBIO DE BLUETOOTH')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (540, 1, N'CAMBIO DE CABLE DC-IN ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (541, 1, N'CAMBIO DE VALVULA-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (542, 1, N'CAMBIO DE BOTON')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (543, 1, N'CAMBIO DE SENSOR DE PUERTA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (544, 1, N'INSTALACION DE LAVADORA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (545, 1, N'CAMBIO DE BLUETOOTH-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (546, 1, N'CAMBIO PINZA DE RESORTE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (547, 1, N'ORIENTACION AL CLIENTE')
GO
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (548, 1, N'CAMBIO DE MOTOR BLOWER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (549, 1, N'CAMBIO DE PANEL FRONTAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (550, 1, N'CAMBIO DE PESTILLO DE PUERTA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (551, 1, N'AJUSTE DE TORNILLO FRONTAL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (552, 1, N'INSTALACION DE PARTE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (553, 1, N'CAMBIO DE PUERTA DE HORNO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (554, 1, N'CAMBIO DE ESPALDAR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (555, 1, N'CAMBO DE PARLANTE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (556, 1, N'INSTALACION DE EQUIPO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (557, 1, N'INSTALACION DE SECADORA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (558, 1, N'CAMBIO DE PANEL PCB')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (559, 1, N'CAMBIO DE RESISTENCIA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (560, 1, N'INTALACION DE BOMBA DE AGUA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (561, 1, N'MOVIMIENTO DE SENSOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (562, 1, N'CAMBIO DE SPIN TIMER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (563, 1, N'AJUSTE DE BOCINA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (564, 1, N'INSTALACION DE EQUIPO-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (565, 1, N'LIMPIEZA DE FIRTRO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (566, 1, N'CAMBIO DE KIT PANEL')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (567, 1, N'CAMBIO DE TORNILLO DE MOTOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (568, 1, N'CAMBIO VALVULA DE GAS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (569, 1, N'PROGRAMACION DE EQUIPO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (570, 1, N'CAMBIO DE REGULADOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (571, 1, N'CAMBIO DE FILTRO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (572, 1, N'AJUSTE DE TEMPERATURA')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (573, 1, N'CAMBIO DE KIT DE TUBERIAS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (574, 1, N'CAMBIO DE CABLE LVS')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (575, 1, N'CAMBIO DE TARJETA SMART')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (576, 1, N'AJUSTE EN RESISTENCIA DE AUDIO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (577, 1, N'AJUSTE DE MECANICO')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (578, 1, N'CAMBIO DE TIRA LED ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (579, 1, N'CAMBIO DE POSICION DE SENSOR')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (580, 1, N'CAMBIO DE FUELLE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (581, 1, N'ESD COMPONENT SWAP')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (582, 1, N'CAMBIO DE IR PCB')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (583, 1, N'CAMBIO DE TAPA ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (584, 1, N'DESINSTALACION DE EQUIPO ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (585, 1, N'CAMBIO DE  BOARD POWER')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (587, 1, N'CAMBIO DE CABLE TP FFC')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (588, 1, N'CAMBIO DE TARJETA DISPLAY-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (589, 1, N'CAMBIO DE BELLOW')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (590, 1, N'CAMBIO DE RESORTE ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (591, 1, N'CAMBIO DE TERMOSTATO-')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (592, 1, N'CAMBIO DE TERMOFUSIBLE')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (593, 1, N'INSPECION DE EQUIPO ')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (594, 1, N'AJUSTE DE PROGRAMACION')
INSERT [dbo].[reparacion] ([repacion_id], [activo], [descripcion_reparacion]) VALUES (595, 1, N'CAMBIO DE TERMO DISCO ')
SET IDENTITY_INSERT [dbo].[reparacion] OFF
SET IDENTITY_INSERT [dbo].[tipo_producto] ON 

INSERT [dbo].[tipo_producto] ([tipo_producto_id], [activo], [Descripcion_tipo_producto]) VALUES (1, 1, N'Lavadora')
INSERT [dbo].[tipo_producto] ([tipo_producto_id], [activo], [Descripcion_tipo_producto]) VALUES (2, 1, N'Nevera')
INSERT [dbo].[tipo_producto] ([tipo_producto_id], [activo], [Descripcion_tipo_producto]) VALUES (3, 1, N'Aire')
INSERT [dbo].[tipo_producto] ([tipo_producto_id], [activo], [Descripcion_tipo_producto]) VALUES (4, 1, N'Televisor')
SET IDENTITY_INSERT [dbo].[tipo_producto] OFF
SET IDENTITY_INSERT [dbo].[usuarios] ON 

INSERT [dbo].[usuarios] ([usuario_id], [Activo], [Usuario], [Clave], [Nombre], [Apellido], [STATUS], [Direccion], [Telefono], [Correo], [fecha_ingreso], [cargo_id]) VALUES (2, 1, N'hsalas', N'113', N'Helmer', N'Salas', 1, N'C/ central # 128 parte atra, Buenos Aires de herrera', N'8095319097', N'yonnich@gmail.com', CAST(0x0000A7D000000000 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[usuarios] OFF
ALTER TABLE [dbo].[clientes]  WITH CHECK ADD FOREIGN KEY([usuario_creador])
REFERENCES [dbo].[usuarios] ([usuario_id])
GO
ALTER TABLE [dbo].[modelo]  WITH CHECK ADD FOREIGN KEY([tipo_producto_id])
REFERENCES [dbo].[tipo_producto] ([tipo_producto_id])
GO
ALTER TABLE [dbo].[ordenOperacion]  WITH CHECK ADD FOREIGN KEY([cliente_id])
REFERENCES [dbo].[clientes] ([cliente_id])
GO
ALTER TABLE [dbo].[ordenOperacion]  WITH CHECK ADD FOREIGN KEY([estado_equipo_id])
REFERENCES [dbo].[estado_equipo] ([estado_equipo_id])
GO
ALTER TABLE [dbo].[ordenOperacion]  WITH CHECK ADD FOREIGN KEY([estado_ubicacion_id])
REFERENCES [dbo].[estado_ubicacion] ([estado_ubicacion_id])
GO
ALTER TABLE [dbo].[ordenOperacion]  WITH CHECK ADD FOREIGN KEY([falla_id])
REFERENCES [dbo].[falla] ([falla_id])
GO
ALTER TABLE [dbo].[ordenOperacion]  WITH CHECK ADD FOREIGN KEY([garantia_id])
REFERENCES [dbo].[garantia] ([garantia_id])
GO
ALTER TABLE [dbo].[ordenOperacion]  WITH CHECK ADD FOREIGN KEY([marca_id])
REFERENCES [dbo].[marca] ([marca_id])
GO
ALTER TABLE [dbo].[ordenOperacion]  WITH CHECK ADD FOREIGN KEY([modelo_id])
REFERENCES [dbo].[modelo] ([modelo_id])
GO
ALTER TABLE [dbo].[ordenOperacion]  WITH CHECK ADD FOREIGN KEY([tipo_producto_id])
REFERENCES [dbo].[tipo_producto] ([tipo_producto_id])
GO
ALTER TABLE [dbo].[ordenoperacion_diagnostico]  WITH CHECK ADD FOREIGN KEY([ordenoperacion_id])
REFERENCES [dbo].[ordenOperacion] ([orden_id])
GO
ALTER TABLE [dbo].[ordenoperacion_diagnostico]  WITH CHECK ADD FOREIGN KEY([marca_id])
REFERENCES [dbo].[marca] ([marca_id])
GO
ALTER TABLE [dbo].[ordenoperacion_diagnostico]  WITH CHECK ADD FOREIGN KEY([modelo_id])
REFERENCES [dbo].[modelo] ([modelo_id])
GO
ALTER TABLE [dbo].[ordenoperacion_diagnostico]  WITH CHECK ADD FOREIGN KEY([producto_id])
REFERENCES [dbo].[tipo_producto] ([tipo_producto_id])
GO
ALTER TABLE [dbo].[ordenoperacion_reparacion]  WITH CHECK ADD FOREIGN KEY([marca_id])
REFERENCES [dbo].[marca] ([marca_id])
GO
ALTER TABLE [dbo].[ordenoperacion_reparacion]  WITH CHECK ADD FOREIGN KEY([modelo_id])
REFERENCES [dbo].[modelo] ([modelo_id])
GO
ALTER TABLE [dbo].[ordenoperacion_reparacion]  WITH CHECK ADD FOREIGN KEY([ordenoperacion_id])
REFERENCES [dbo].[ordenOperacion] ([orden_id])
GO
ALTER TABLE [dbo].[ordenoperacion_reparacion]  WITH CHECK ADD FOREIGN KEY([producto_id])
REFERENCES [dbo].[tipo_producto] ([tipo_producto_id])
GO
ALTER TABLE [dbo].[ordenoperacion_reparacion]  WITH CHECK ADD FOREIGN KEY([reparacion_id])
REFERENCES [dbo].[reparacion] ([repacion_id])
GO
ALTER TABLE [dbo].[producto]  WITH CHECK ADD FOREIGN KEY([tipo_producto_id])
REFERENCES [dbo].[tipo_producto] ([tipo_producto_id])
GO
ALTER TABLE [dbo].[usuarios]  WITH CHECK ADD FOREIGN KEY([cargo_id])
REFERENCES [dbo].[Cargo] ([cargo_id])
GO
