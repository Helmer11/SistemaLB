﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaLineaB.DataBase;

namespace SistemaLineaB.Clases
{
    class Articulo
    {
        ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();

        public object CargarServicio()
        {

            var ob = (from q in db.Servicios
                      select q).ToList();

            return ob;

        }
        public object CargarServicio(string articulo)
        {

            var ob = (from q in db.Servicios
                      where q.marca.Contains(articulo)
                      select q).ToList();

            return ob;

        }




    }
}
