﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaLineaB.DataBase;

namespace SistemaLineaB.Clases
{
    class Roles_Validacion
    {

        public bool Rol_usuario(int usuario_id)
        {
            ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();

            var cargoId = (from q in db.Usuario_Roles
                        where q.usuario_id.Equals(usuario_id)
                        select q.cargo_id).ToList();

            var roll = cargoId.Contains(usuario_id);

            if(roll == true)
            {
                return true;
            }else
            {
                return false;
            }
        }

    }
}
