﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaLineaB.DataBase;


namespace SistemaLineaB.Clases
{
    class RCliente
    {
    


        public Boolean CrearCliente(String nombre, String apellido, string cedula, string rnc,
            string direccion, string telefono1, string telefono2, string correo, int usuario_creador, DateTime fecha_ingreso)
        {

            try
            {
                ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();
                cliente cliente = new cliente();

                cliente.Nombre = nombre;
                cliente.Apellido = apellido;
                cliente.Cedula = cedula;
                cliente.RNC = rnc;
                cliente.Direccion = direccion;
                cliente.Telefono_1 = telefono1;
                cliente.Telefono_2 = telefono2;
                cliente.Correo = correo;
                cliente.usuario_creador = usuario_creador;
                cliente.Fecha_ingreso = fecha_ingreso;

                db.clientes.InsertOnSubmit(cliente);
                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false ;
               
            }

        }
        public Boolean ActualidacionCliente(int codigo,String nombre, String apellido, string cedula, string rnc,
            string direccion, string telefono1, string telefono2, string correo, int usuario_creador, DateTime fecha_ingreso)

        {
            try
            {
              
                ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();
                cliente cliente = new cliente();

                var client = db.clientes.Where(z => z.cliente_id.Equals(codigo)).FirstOrDefault();

                client.Nombre = nombre;
                client.Apellido = apellido;
                client.Cedula = cedula;
                client.RNC = rnc;
                client.Direccion = direccion;
                client.Telefono_1 = telefono1;
                client.Telefono_2 = telefono2;
                client.Correo = correo;
                client.usuario_creador = usuario_creador;
                client.Fecha_ingreso = fecha_ingreso;
                
                db.SubmitChanges();

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public Boolean EliminarCliente(long codigo )
        {

            try
            {
                ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();

                var EliminarCliente = (from m in db.clientes
                             where m.cliente_id.Equals(codigo)
                             select m).FirstOrDefault();

      
                db.clientes.DeleteOnSubmit(EliminarCliente);
                db.SubmitChanges();

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public object BuscarCliente()
        {
            ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();
            var client = (from t in db.vw_Clientes
                          select t).ToList();
            return client;

        }
        public object BuscarCliente(string cliente)
        {
            ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();
            var client = (from t in db.vw_Clientes
                          where t.Nombre.Contains(cliente)
                          select t).ToList();


            return client;

        }



    }

}
