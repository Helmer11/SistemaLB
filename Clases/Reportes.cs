﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaLineaB.DataBase;
namespace SistemaLineaB.Clases
{
    class Reportes
    {
        ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();

        public object CargarReporte()
        {
           
            var query = (from t in db.Detalle_reports
                         select t).ToList();

            return query;
        }

        public object CargarReporte(string orden="",string serie = "",string cliente = "" ,string estadoEquipo = "",string tipoProducto="",
           string marca = "",string fechaFabricante = "",string FechaVenta="" )
        {

            var query = (from t in db.Detalle_reports
                         where t.ORDEN_SERVICIO.Equals(orden) | t.SERIE.Equals(serie) | t.CLIENTE.Equals(cliente)
                         | t.ESTADO_EQUIPO.Equals(estadoEquipo) | t.TIPO_PRODUCTO.Equals(tipoProducto) | t.MARCAS.Equals(marca)
                         select t).ToList();

            return query;

          
        }




            



    }
}
