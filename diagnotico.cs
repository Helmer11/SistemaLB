﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.DataBase;
using SistemaLineaB.Clases;


namespace SistemaLineaB
{
    public partial class diagnotico : Form
    {

        ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();
        public diagnotico()
        {
            InitializeComponent();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            int orden = 0;
            if (txtorden.Text == "")
            {
                MessageBox.Show("Introduzca una orden");
            }
            else
            {
                orden = Convert.ToInt32(txtorden.Text);
                buscarOrden(orden);
            }
        }

        public void buscarOrden(int orden)
        {
            var query = (from d in db.ordenOperacions
                         where d.orden_id.Equals(orden) && d.estado_equipo_id.Equals(2)
                         select d).FirstOrDefault();


            if(query != null)
            {
                cbProducto.SelectedText = query.tipo_producto.Descripcion_tipo_producto;
                txtmarca.Text = query.marca.nombre_marca;
                txtmodelo.Text = query.modelo.nombre_modelo;
                txtserie.Text = query.serie;
                CbFechaVenta.SelectedText = query.fecha_venta.ToString();
                lbEstado.Text =  query.estado_equipo.descripcion.ToUpper();
            }else
            {
                MessageBox.Show("Esta Orden no se encuentra en estado de Diagnostico");
            }

            
        }

        public void carga_falla()
        {
            cbxfalla.DropDownStyle = ComboBoxStyle.DropDownList;
            var falla = (from k in db.fallas
                         select k).ToList();

            cbxfalla.DataSource = falla;
            cbxfalla.DisplayMember = "descripcion_falla";
            cbxfalla.ValueMember = "falla_id";
        }

             public void carga_garantia()
        {
            cbxGarantia.DropDownStyle = ComboBoxStyle.DropDownList;
            var garantia = (from w in db.tipo_garantias
                         select w).ToList();

            cbxGarantia.DataSource = garantia;
            cbxGarantia.DisplayMember = "descripcion_garantia";
            cbxGarantia.ValueMember = "tipo_garantia_id";
        }



        private void diagnotico_Load(object sender, EventArgs e)
        {
            carga_falla();
            carga_garantia();
            this.Desabilitado(false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
             Boolean  guardar = GuardarDiagnostico();

            if (guardar == true)
            {
                MessageBox.Show("El diagnostico fue guardado");
            }
            else
            {
                MessageBox.Show("El diagnostico no pudo ser guardado");
            }

        }

        public Boolean GuardarDiagnostico()
        {

            try
            {
                int ordenID = int.Parse(txtorden.Text);

                var query = db.ordenOperacions.Where(a => a.orden_id.Equals(ordenID)).FirstOrDefault();

                ordenoperacion_diagnostico od = new ordenoperacion_diagnostico();

                od.ordenoperacion_id = query.orden_id;     // Convert.ToInt32(txtorden.Text);
                od.producto_id = query.tipo_producto_id; // Convert.ToInt32(cbProducto.SelectedValue);
                od.marca_id = query.marca_id;  // Convert.ToInt32(txtmarca.Text);
                od.modelo_id = query.modelo_id; //Convert.ToInt32(txtmodelo.Text);
                od.serie = txtserie.Text;
                od.fecha_venta = query.fecha_venta; //Convert.ToDateTime(CbFechaVenta.SelectedText);
                od.garantia = cbxGarantia.SelectedText;
                od.falla_id = Convert.ToInt32(cbxfalla.SelectedValue);
                od.comentario = txtcomentario.Text;

                db.ordenoperacion_diagnosticos.InsertOnSubmit(od);
                db.SubmitChanges();

                return true;
            }catch(Exception ex)
            {
                return false;
            }

        }
        public void Desabilitado( bool estado)
        {

            cbProducto.Enabled = estado;
            txtmarca.Enabled = estado;
            txtmodelo.Enabled = estado;
            txtserie.Enabled = estado;
            CbFechaVenta.Enabled = estado;

        }
    }
}
