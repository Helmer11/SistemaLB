﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.Clases;
using SistemaLineaB.DataBase;

namespace SistemaLineaB
{
    public partial class reporte : Form
    {
        public reporte()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string orden;
            string serie;
            string cliente;
            string marca;
            string FechaDesde;
            string fechaHasta;
            string estadoEquipo;
            string tipoProducto;

         if(txtOrdenServicio.Text == "")
            {
                orden = txtOrdenServicio.Text;

            }
            else
            {
                orden = txtOrdenServicio.Text;

            }
            if(txtSerie.Text == "")
            {
                serie = txtSerie.Text;
            }
            else
            {
                serie = txtSerie.Text;
            }
            if(txtCliente.Text == "")
            {
                cliente = txtCliente.Text;
            }
            else
            {
                cliente = txtCliente.Text;
            }
            if(txtMarca.Text == "")
            {
                marca = txtMarca.Text;
            }
            else
            {
                marca = txtMarca.Text;
            }
            if(cbEstadoEquipo.Text == "")
            {
                estadoEquipo = cbEstadoEquipo.SelectedItem.ToString();
            }
            else
            {
                estadoEquipo = cbEstadoEquipo.SelectedItem.ToString();
            }
            if(cbTipoProducto.Text == "")
            {
                tipoProducto = cbTipoProducto.SelectedItem.ToString();
            }
            else
            {
                tipoProducto = cbTipoProducto.SelectedItem.ToString();
            }

            if(DtpFechaDesde.Text == "")
            {
                FechaDesde = DtpFechaDesde.Text;
            }
            else
            {
                FechaDesde = DtpFechaDesde.Text;
            }
            if(DtpFechaHasta.Text == "")
            {
                fechaHasta = DtpFechaHasta.Text;
            }
            else
            {
                fechaHasta = DtpFechaHasta.Text;
            }

            Reportes rp = new Reportes();
           var datos = rp.CargarReporte(orden, serie, cliente, estadoEquipo, tipoProducto, marca);

            if (datos.Equals(0))
            {
                MessageBox.Show("Su Criterio de busqueda no fue encontrado");
            }
            else
            {
                dgvReporte.DataSource = datos;
            }


           



        }

        private void Exportar_Click(object sender, EventArgs e)
        {

        }

        private void reporte_Load(object sender, EventArgs e)
        {
            Reportes rp = new Reportes();
             dgvReporte.DataSource=rp.CargarReporte();
            CargarEstadoEquipo();
            CargarTipoProducto();
        }




        private void CargarEstadoEquipo()
        {
            cbEstadoEquipo.DropDownStyle = ComboBoxStyle.DropDownList;
           
            ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();
            cbEstadoEquipo.DataSource = (from t in db.estado_equipos
                                         select t.descripcion).ToList();

        }
        private void CargarTipoProducto()
        {
            cbTipoProducto.DropDownStyle = ComboBoxStyle.DropDownList;
            ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();
            cbTipoProducto.DataSource = (from w in db.tipo_productos
                                         select w.Descripcion_tipo_producto).ToList();
        }

      
    }
}
