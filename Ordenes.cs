﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.DataBase;
using SistemaLineaB.Clases;


namespace SistemaLineaB
{
    public partial class Ordenes : Form
    {

        ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();


        public Ordenes()
        {
            InitializeComponent();
        }

        private void Ordenes_Load(object sender, EventArgs e)
        {
            cargar_tipo_producto();
            cargar_marca();
            cargar_modelo();

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            int codigo = Convert.ToInt32(txtcodigo.Text);
            BuscarOrden(codigo);
        }

        public void BuscarOrden(int codigoCliente)
        {
            var buscar = (from p in db.clientes
                          where p.cliente_id.Equals(codigoCliente)
                          select p).FirstOrDefault();


            if(buscar != null)
            {
                txtcodigo.Text = Convert.ToString(buscar.cliente_id);
                txtNombre.Text = buscar.Nombre;
                txtApellido.Text = buscar.Apellido;
                txtDireccion.Text = buscar.Direccion;
                txtRNC.Text = buscar.RNC;
                txtTelefono.Text = buscar.Telefono_1;
                txtCedula.Text = buscar.Cedula;
                txtCelular.Text = buscar.Telefono_2;
                txtCorreo.Text = buscar.Correo;

            }
            else
            {
                MessageBox.Show("El cliente no existe");
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            GuardarOrden();
        }


        public void GuardarOrden()
        {
            string mensaje = "";
            bool client;
            ordenOperacion op = new ordenOperacion();
            RCliente ct = new RCliente();
            Funciones f = new Funciones();
            mensaje =  Validacion();

            if(!string.IsNullOrEmpty(mensaje))
            {
                MessageBox.Show(mensaje);
                return;   
            }
            else
            {
                var clienteID = (from c in db.clientes
                          where c.Nombre.Equals(txtNombre.Text)
                          select c.cliente_id).FirstOrDefault();

                if (clienteID > 0)
                {

                }
                else
                {
                    client = ct.CrearCliente(txtNombre.Text, txtApellido.Text, txtCedula.Text, txtRNC.Text, txtDireccion.Text,
                   txtTelefono.Text, txtCelular.Text, txtCorreo.Text, f.usuarioID, DateTime.Now);

                    if (client == false)
                    {
                        MessageBox.Show("El cliente no pudo ser guardado");

                    }
                }
            }

            var orden = db.ordenOperacions.Max(s => s.orden_id );

            var clienteid = db.clientes.Where(x => x.Nombre.Equals(txtNombre.Text)).Select(z => z.cliente_id).FirstOrDefault();

            if (orden.Equals(null)){
                MessageBox.Show("nadadada ");
            }
            else
            {
                try
                {
                    op.activo = true;
                    op.marca_id = (int)cbxMarca.SelectedValue;
                    op.modelo_id = (int)cbxModelo.SelectedValue;
                    op.cliente_id =clienteid ;
                    op.serie = txtSerie.Text;
                    op.falla_id = 19;
                    op.fecha_fabricante = Convert.ToDateTime(DtpFechaProducto.Text);
                    op.fecha_venta = Convert.ToDateTime(DtpFechaVenta.Text);
                    op.tipo_producto_id = (int)cbxProducto.SelectedValue;
                    op.garantia_id = 3;
                    op.estado_equipo_id = 1;
                    op.estado_ubicacion_id = 1;
                    db.ordenOperacions.InsertOnSubmit(op);
                    db.SubmitChanges();
                    MessageBox.Show("Orden Creada Satisfatoriamente");
                    var ultimaOrden = db.ordenOperacions.Max(z => z.orden_id);

                    txtOrden.Text = ultimaOrden.ToString();
                    this.Limpiar();

                   



                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            
        }

        private void Limpiar()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtCedula.Clear();
            txtCelular.Clear();
            txtRNC.Clear();
            txtSerie.Clear();
            txtCorreo.Clear();
            txtDireccion.Clear();
            txtTelefono.Clear();
        }





        private void cargar_modelo()
        {
            var modelo = from m in db.modelos
                         orderby m.nombre_modelo
                         select m;

           
            cbxModelo.DataSource = modelo;
            cbxModelo.DisplayMember = "nombre_modelo";
            cbxModelo.ValueMember = "modelo_id";

        }
       private void cargar_marca()
        {
            var marca = from mc in db.marcas
                        orderby mc.nombre_marca
                        select mc;

           
            cbxMarca.Items.Insert(0,"MARCA");
            cbxMarca.DataSource = marca;
            cbxMarca.DisplayMember = "nombre_marca";
            cbxMarca.ValueMember = "marca_id";

        }
        private void cargar_tipo_producto()
        {
            var tipoProducto = from tp in db.tipo_productos
                               orderby tp.Descripcion_tipo_producto
                               select tp;

            cbxProducto.Items.Insert(0, "Tipo Producto");
            cbxProducto.DataSource = tipoProducto;
            cbxProducto.DisplayMember = "Descripcion_tipo_producto";
            cbxProducto.ValueMember = "tipo_producto_id";

        }

        private string Validacion()
        {
            string mensaje = "";

            if (txtNombre.Text == "")
            {
                mensaje = "Por favor escribir el nombre del cliente";
            }
            else if (txtApellido.Text == "")
            {
                mensaje += "Por favor escriba el apellido del cliente";
            }
            else if (txtCedula.Text == "")
            {
                mensaje += "Por favor escriba el cedula del cliente";
            }
            else if (txtRNC.Text == "")
            {
                mensaje += "Por favor escriba el RNC del cliente";
            }
            else if(txtTelefono.Text == "")
            {
                mensaje += "Por favor escriba el Telefono del cliente";
            }
            else if(txtCelular.Text == "")
            {
                mensaje += "Por favor escriba el celular del cliente";
            }
            else if( txtCorreo.Text == "")
            {
                mensaje += "Por favor escriba el correo del cliente";
            }
            else if (txtDireccion.Text == "")
            {
                mensaje += "Por favor escriba el direccion del cliente";
            }
            else if (cbxMarca.SelectedValue.Equals(0))
            {
                mensaje += "Por favor seleccione la marca";
            }
            else if (cbxModelo.SelectedValue.Equals(0))
            {
                mensaje += "Por favor seleccione el modelo ";
            }
            else if (cbxProducto.SelectedValue.Equals(0))
            {
                mensaje += "Por favor seleccione el tipo de producto";
            }
            return mensaje;
           
        }

    }
}
