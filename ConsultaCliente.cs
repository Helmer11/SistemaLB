﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.Clases;
using System.Windows.Forms;

namespace SistemaLineaB
{
    public partial class ConsultaCliente : Form
    {
     

        public ConsultaCliente()
        {
            InitializeComponent();
        }

        private void ConsultaCliente_Load(object sender, EventArgs e)
        {
            RCliente ccliente = new RCliente();
            dgvCliente.DataSource= ccliente.BuscarCliente();
        }

       

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(System.Text.RegularExpressions.Regex.IsMatch(txtBusquedaCliente.Text,"[0-1]"))
            {
                MessageBox.Show("Introduzca el nombre del cliente para la busqueda");
                return;
            }

            if(txtBusquedaCliente.Text == "")
            {
                RCliente ccliente = new RCliente();
                dgvCliente.DataSource = ccliente.BuscarCliente();
            }
            else
            {
                RCliente ct = new RCliente();
               
                string client_id = txtBusquedaCliente.Text.Trim();
                var BusquedaCliente = ct.BuscarCliente(client_id);

                if(BusquedaCliente == null)
                {
                    MessageBox.Show("El cliente no existe");
                }

                dgvCliente.DataSource = BusquedaCliente;
            }
            

        }
    }
}
