﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.DataBase;
using SistemaLineaB.Clases;

namespace SistemaLineaB
{
    public partial class RecepcionEquipo : Form
    {


        ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();

        public RecepcionEquipo()
        {
            InitializeComponent();
        }


        public void buscarOrden(int orden)
        {
            var query = (from d in db.ordenOperacions
                         where d.orden_id.Equals(orden) && d.estado_equipo.Equals(EstadoEquipo.Despachado)
                         select d).FirstOrDefault();

            if (query != null)
            {
                txtproducto.Text = query.tipo_producto.Descripcion_tipo_producto;
                txtmarca.Text = query.marca.nombre_marca;
                txtmodelo.Text = query.modelo.nombre_modelo;
                txtserie.Text = query.serie;
               
            }
            else
            {
                MessageBox.Show("No existe orden con su criterio");
            }


     
    }
        public enum EstadoEquipo
        {
            
            Despachado = 4,
            RecepcionEquipo = 5,
        }
        public Boolean GuardarRecepcion()
        {

            try
            {
                ordenoperacion_diagnostico od = new ordenoperacion_diagnostico();
                ordenOperacion op = new ordenOperacion();

                txtproducto.Text = op.tipo_producto.Descripcion_tipo_producto;
                txtmarca.Text = op.marca.nombre_marca;
                txtmodelo.Text = op.modelo.nombre_modelo;
                txtserie.Text = op.serie;

                op.estado_equipo_id = (int)EstadoEquipo.RecepcionEquipo;
                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        private void btnRecepcionEquipo_Click(object sender, EventArgs e)
        {
          bool recepcion= GuardarRecepcion();

            if (recepcion)
            {
                MessageBox.Show("La recepcion fue realizada");
            }else
            {
                MessageBox.Show("No se pudo rececionar el equipo. Por favor intentelo de nuevo");
            }
        }
    }




}
