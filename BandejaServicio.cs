﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.DataBase;
using SistemaLineaB.Clases;


namespace SistemaLineaB
{
    public partial class BandejaServicio : Form
    {
      
        
        public BandejaServicio()
        {
            InitializeComponent();
        }

        private void BandejaServicio_Load(object sender, EventArgs e)
        {
            Articulo at = new Articulo();
            var Datos_bandejaServicio = at.CargarServicio();

            dgvBandejaServicio.DataSource = Datos_bandejaServicio;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Articulo at = new Articulo();
            

            if (System.Text.RegularExpressions.Regex.IsMatch(txtArticulo.Text, "[0-1]"))
            {
                var bandejaServicio = at.CargarServicio();
                dgvBandejaServicio.DataSource = bandejaServicio;
            }
            else
            {
                string artic = txtArticulo.Text;
               
                dgvBandejaServicio.DataSource = at.CargarServicio(artic);
            }
        }


        private void txtArticulo_TextChanged(object sender, EventArgs e)
        {
            Articulo at = new Articulo();
            if (System.Text.RegularExpressions.Regex.IsMatch(txtArticulo.Text, "[0-1]"))
            {
                dgvBandejaServicio.DataSource = at.CargarServicio();
            }
            else
            {
                string articul = txtArticulo.Text;
                var dataArticulo = at.CargarServicio(articul);
                dgvBandejaServicio.DataSource = dataArticulo;
            }
        }
    }
}
