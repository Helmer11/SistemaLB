﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaLineaB.DataBase;
using SistemaLineaB.Clases;

namespace SistemaLineaB
{
    public partial class reparacion : Form
    {
        ElectrodomesticoDataContext db = new ElectrodomesticoDataContext();

        public reparacion()
        {
            InitializeComponent();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            int codigo = 0;
            if (txtorden.Text == "")
            {
                MessageBox.Show("Introduzca una orden");
            }
            else
            {
                codigo = Convert.ToInt32(txtorden.Text);
                buscarOrden(codigo);
            } 
        }
        public void buscarOrden(int orden)
        {
            var query = (from d in db.ordenOperacions
                         where d.orden_id.Equals(orden) && d.estado_equipo_id.Equals(3)
                         select d).FirstOrDefault();


            if(query  != null) 
            {
                cbProducto.Text = query.tipo_producto.Descripcion_tipo_producto;
                txtmarca.Text = query.marca.nombre_marca;
                txtmodelo.Text = query.modelo.nombre_modelo;
                txtserie.Text = query.serie;
                CbFechaVenta.SelectedText = query.fecha_venta.ToString();
                lbEstado.Text =  query.estado_equipo.descripcion.ToUpper();
                lbUbicacion.Text = query.estado_ubicacion.descripcion.ToUpper();
            }
            else{
                MessageBox.Show("No existe criterio con su busqueda");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Boolean guardar = GuardarReparacion();

            if (guardar == true)
            {
                MessageBox.Show("El diagnostico fue guardado");
            }
            else
            {
                MessageBox.Show("El diagnostico no pudo ser guardado");
            }

        }
       

        public Boolean GuardarReparacion()
        {

            try
            {
                int ordenID = int.Parse(txtorden.Text);
                var query = db.ordenOperacions.Where(z => z.orden_id.Equals(ordenID)).FirstOrDefault();

                ordenoperacion_reparacion or = new ordenoperacion_reparacion();

                or.ordenoperacion_id = query.orden_id; //Convert.ToInt32(txtorden.Text);
                or.producto_id = query.tipo_producto_id; //Convert.ToInt32(cbProducto.SelectedText);
                or.marca_id = query.marca_id;  // Convert.ToInt32(txtmarca.Text);
                or.modelo_id = query.modelo_id; //Convert.ToInt32(txtmodelo.Text);
                or.serie = txtserie.Text;
                or.fecha_venta = query.fecha_venta;// Convert.ToDateTime(CbFechaVenta.SelectedText);
                        or.cambio = rbcambio.Checked;
                        or.software = rbsoftware.Checked;
                        or.no_reparado = rbno_reparado.Checked;
                        or.para_trasladar = rb_paraTrasladar.Checked;
                        or.reparacion_id =(int) cbxreparacion.SelectedValue;
                        or.parte_id = cbxparte.SelectedIndex;
                or.comentario = txtcomentario.Text;

                db.ordenoperacion_reparacions.InsertOnSubmit(or);
                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

}
       public void cargar_parte()
        {
            cbxparte.DropDownStyle = ComboBoxStyle.DropDownList;
            var parte = from f in db.partes
                        select f;

            cbxparte.DataSource = parte;
            cbxparte.DisplayMember = "Descripcion_parte";
            cbxparte.ValueMember = "parte_id";
        }
        public void cargar_reparacion()
        {
            cbxreparacion.DropDownStyle = ComboBoxStyle.DropDownList;
            var reparacion = from w in db.reparacions
                        select w;

            cbxreparacion.DataSource = reparacion;
            cbxreparacion.DisplayMember = "Descripcion_reparacion";
          cbxreparacion.ValueMember = "repacion_id";
        }

        private void reparacion_Load(object sender, EventArgs e)
        {
            cargar_parte();
            cargar_reparacion();
            this.Desabilitado(false);
        }
        public void Desabilitado(bool estado)
        {

            cbProducto.Enabled = estado;
            txtmarca.Enabled = estado;
            txtmodelo.Enabled = estado;
            txtserie.Enabled = estado;
            CbFechaVenta.Enabled = estado;

        }
    }
}
